package it.hiree.register.hook.action;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import it.hiree.register.model.Student;
import it.hiree.register.model.University;
import it.hiree.register.service.StudentLocalServiceUtil;
import it.hiree.register.service.UniversityLocalServiceUtil;

/**
 * @author trungnt
 *
 */

@Component(immediate = true, property = { "javax.portlet.name=com_liferay_login_web_portlet_LoginPortlet",
		"mvc.command.name=/register/action" }, service = MVCActionCommand.class)
public class RegisterMVCActionCommand extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);

		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);
		try {
			if (cmd.equals("CUSTOM_REGISTER")) {
				String userName = ParamUtil.getString(actionRequest, "userName");
				String secret = ParamUtil.getString(actionRequest, "secret");
				String authURL = ParamUtil.getString(actionRequest, "universityUrl");
				boolean authRequired = ParamUtil.getBoolean(actionRequest, "authRequired");

				int statusCode = 0;

				JSONObject dataResponse = JSONFactoryUtil.createJSONObject();

				if (authRequired) {
					statusCode = auth(authURL, userName, secret);
					
					if (statusCode == 200) {
						dataResponse = register(actionRequest, actionResponse);
						dataResponse.put("authorization", "Basic Auth");
						dataResponse.put("statusCode", statusCode);
						dataResponse.put("userName", userName);
						dataResponse.put("password", secret);
						dataResponse.put("authenticationURL", authURL);
					} else {
						dataResponse.put("authorization", "Basic Auth");
						dataResponse.put("statusCode", statusCode);
						dataResponse.put("userName", userName);
						dataResponse.put("password", secret);
						dataResponse.put("authenticationURL", authURL);
					}
				} else {
					dataResponse.put("authorization", "none");
					dataResponse = register(actionRequest, actionResponse);
				}

				PrintWriter out = response.getWriter();
				out.print(dataResponse.toJSONString());
				out.flush();
				out.close();

			} else {
				mvcActionCommand.processAction(actionRequest, actionResponse);
			}
		} catch (Exception e) {
			_log.error(e);
			PrintWriter out = response.getWriter();
			JSONObject dataResponse = JSONFactoryUtil.createJSONObject();
			dataResponse.put("message", e.getMessage());
			out.print(dataResponse.toJSONString());
			out.flush();
			out.close();

		}
	}

	private JSONObject register(ActionRequest actionRequest, ActionResponse actionResponse)
			throws SystemException, PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Company company = themeDisplay.getCompany();
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
		JSONObject dataResponse = JSONFactoryUtil.createJSONObject();

		String name = ParamUtil.getString(actionRequest, "name");
		String surName = ParamUtil.getString(actionRequest, "surName");
		String password = ParamUtil.getString(actionRequest, "password");
		String emailAddress = ParamUtil.getString(actionRequest, "emailAddress");
		int finished = ParamUtil.getInteger(actionRequest, "finished");
		String university = ParamUtil.getString(actionRequest, "university");
		String universityCode = ParamUtil.getString(actionRequest, "universityCode");
		String universityName = ParamUtil.getString(actionRequest, "universityName");
		String course = ParamUtil.getString(actionRequest, "course");

		Student student = StudentLocalServiceUtil.createStudent(themeDisplay.getUserId(),
				themeDisplay.getScopeGroupId(), company.getCompanyId(), name, surName, emailAddress, password,
				universityName, Validator.isNotNull(university) ? university : universityCode, course,
				finished == 1 ? true : false, serviceContext);

		long mappingUserId = student.getMappingUserId();

		User mappingUser = UserLocalServiceUtil.fetchUser(mappingUserId);

		University universityModel = UniversityLocalServiceUtil
				.fetchByGroupId_UniversityCode(themeDisplay.getScopeGroupId(), student.getUniversityCode());

		request.getSession().setAttribute("_USER_LOGIN", mappingUserId);
		dataResponse.put("id", student.getStudentId());
		dataResponse.put("name", student.getName());
		dataResponse.put("surName", student.getSurName());
		dataResponse.put("emailAddress", mappingUser != null ? mappingUser.getEmailAddress() : "");
		dataResponse.put("mappingUserId", mappingUserId);
		dataResponse.put("universityCode", student.getUniversityCode());
		dataResponse.put("universityName", universityModel != null ? universityModel.getName() : "");

		return dataResponse;
	}

	private int auth(String uri, String userName, String password) {

		HttpURLConnection conn = null;
		int code = 0;
		try {
			String authStr = userName + ":" + password;
			String base64Creds = Base64.getEncoder().encodeToString(authStr.getBytes());

			URL url = new URL(uri);

			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("Authorization", "Basic " + base64Creds);
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			
			conn.setInstanceFollowRedirects(true);
			HttpURLConnection.setFollowRedirects(true);
			conn.setReadTimeout(60 * 1000);
			System.out.println(conn.getRequestProperties());
			for (String header : conn.getRequestProperties().keySet()) {
				if (header != null) {
					for (String value : conn.getRequestProperties().get(header)) {
						System.out.println(header + ":" + value);
					}
				}
			}
			
			conn.connect();
			code = conn.getResponseCode();
			/*
			 * System.out.println("Basic " + base64Creds); Map<String, List<String>> map =
			 * conn.getHeaderFields(); for (Map.Entry<String, List<String>> entry :
			 * map.entrySet()) { String values = ""; for (String value : entry.getValue()) {
			 * values += value + ","; } System.out.println("Key : " + entry.getKey() +
			 * " Value : " + values); }
			 */
			
		} catch (Exception e) {
			_log.error(e);
			return 500;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		return code;
	}

	private Log _log = LogFactoryUtil.getLog(RegisterMVCActionCommand.class);

	@Reference(target = "(&(mvc.command.name=/login/login)(javax.portlet.name=com_liferay_login_web_portlet_LoginPortlet))")
	protected MVCActionCommand mvcActionCommand;

}