package it.hiree.register.hook.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.security.auto.login.BaseAutoLogin;
import com.liferay.portal.kernel.service.UserLocalService;

/**
 * @author trungnt
 *
 */
@Component(immediate = true, service = AutoLogin.class)
public class RegisterAutoLogin extends BaseAutoLogin {

	@Override
	protected String[] doLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = getUser(request);

		if (user == null) {
			return null;
		}

		String[] credentials = new String[3];

		credentials[0] = String.valueOf(user.getUserId());
		credentials[1] = user.getPassword();
		credentials[2] = Boolean.FALSE.toString();

		return credentials;
	}

	protected User getUser(HttpServletRequest request) throws PortalException {

		HttpSession session = request.getSession();

		Long userLogin = (Long) session.getAttribute(_SESSION_API_PRIFIX + "_USER_LOGIN");
		
		if(userLogin == null) {
			return null;
		}

		return _userLocalService.fetchUser(userLogin);
	}

	@Reference(unbind = "-")
	protected void setUserLocalService(UserLocalService userLocalService) {
		_userLocalService = userLocalService;
	}
	private String _SESSION_API_PRIFIX = "equinox.http.login-web";
	private UserLocalService _userLocalService;
}
