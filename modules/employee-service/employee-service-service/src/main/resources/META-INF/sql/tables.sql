create table EMPLOYEE_Employee (
	uuid_ VARCHAR(75) null,
	employeeId VARCHAR(75) not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	email VARCHAR(75) null,
	gender INTEGER,
	description VARCHAR(75) null
);
