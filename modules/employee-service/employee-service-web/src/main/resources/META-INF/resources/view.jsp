<%@ include file="/init.jsp" %>

<%
	PortletURL portletURL = renderResponse.createRenderURL();

	String displayStyle = ParamUtil.getString(request, "displayStyle");

	ViewEmployeeManagementToolbarDisplayContext displayContext =
			new ViewEmployeeManagementToolbarDisplayContext(request, renderRequest, renderResponse, displayStyle);

%>

<clay:management-toolbar
		componentId="viewEmployeeToolbar"
		creationMenu="<%=  displayContext.getCreationMenu() %>"
		searchContainerId="roleSearch"
		searchFormName="searchFm"
		selectable="<%= false %>"
		showCreationMenu="<%= true %>"
		showSearch="<%= true %>"
/>

<liferay-ui:search-container
		curParam="cur1"
		deltaConfigurable="<%= true %>"
		emptyResultsMessage="there-are-not-any-employee-now"
		iteratorURL="<%= portletURL %>"
		total="<%= EmployeeLocalServiceUtil.countEmployee() %>"
		id="employee"
		delta="5"
>
	<liferay-ui:search-container-results
			results="<%= EmployeeLocalServiceUtil.searchEmployee(searchContainer.getStart(), searchContainer.getEnd()) %>"
	/>

	<liferay-ui:search-container-row
			className="com.poc.employee.service.model.Employee"
			escapedModel="<%= true %>"
			keyProperty="employeeId"
			modelVar="curCategory"
	>
		<liferay-portlet:renderURL varImpl="rowURL">
			<portlet:param name="mvcPath" value="/edit_employee.jsp" />
		</liferay-portlet:renderURL>

		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="first-name"
				value="<%= curCategory.getFirstName() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="last-name"
				value="<%= curCategory.getLastName() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="email"
				value="<%= curCategory.getEmail() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="description"
				value="<%= curCategory.getDescription() %>"
		/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator
			markupView="lexicon"
			type="more"
	/>
</liferay-ui:search-container>
