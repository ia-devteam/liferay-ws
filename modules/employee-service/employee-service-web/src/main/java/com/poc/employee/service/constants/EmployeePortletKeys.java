package com.poc.employee.service.constants;

/**
 * @author Khoa VU
 */
public class EmployeePortletKeys {

	public static final String EMPLOYEE =
		"com_poc_employee_service_EmployeePortlet";
}