package com.poc.employee.service.utils;

import com.liferay.portal.kernel.security.permission.ActionKeys;
/**
 * @author Khoa VU
 */
public class CustomActionKeys extends ActionKeys {
    public static final String ADD_EMPLOYEE = "ADD_EMPLOYEE";
}
