/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Student. This utility wraps
 * <code>it.hiree.register.service.impl.StudentLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see StudentLocalService
 * @generated
 */
@ProviderType
public class StudentLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.hiree.register.service.impl.StudentLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the student to the database. Also notifies the appropriate model listeners.
	 *
	 * @param student the student
	 * @return the student that was added
	 */
	public static it.hiree.register.model.Student addStudent(
		it.hiree.register.model.Student student) {

		return getService().addStudent(student);
	}

	public static void addUniversityStudent(long universityId, long studentId) {
		getService().addUniversityStudent(universityId, studentId);
	}

	public static void addUniversityStudent(
		long universityId, it.hiree.register.model.Student student) {

		getService().addUniversityStudent(universityId, student);
	}

	public static void addUniversityStudents(
		long universityId,
		java.util.List<it.hiree.register.model.Student> students) {

		getService().addUniversityStudents(universityId, students);
	}

	public static void addUniversityStudents(
		long universityId, long[] studentIds) {

		getService().addUniversityStudents(universityId, studentIds);
	}

	public static void clearUniversityStudents(long universityId) {
		getService().clearUniversityStudents(universityId);
	}

	/**
	 * Creates a new student with the primary key. Does not add the student to the database.
	 *
	 * @param studentId the primary key for the new student
	 * @return the new student
	 */
	public static it.hiree.register.model.Student createStudent(
		long studentId) {

		return getService().createStudent(studentId);
	}

	public static it.hiree.register.model.Student createStudent(
			long userId, long groupId, long companyId, String name,
			String surName, String emailAddress, String password,
			String universityName, String universityCode, String course,
			boolean finished,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return getService().createStudent(
			userId, groupId, companyId, name, surName, emailAddress, password,
			universityName, universityCode, course, finished, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the student
	 * @return the student that was removed
	 * @throws PortalException if a student with the primary key could not be found
	 */
	public static it.hiree.register.model.Student deleteStudent(long studentId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteStudent(studentId);
	}

	/**
	 * Deletes the student from the database. Also notifies the appropriate model listeners.
	 *
	 * @param student the student
	 * @return the student that was removed
	 */
	public static it.hiree.register.model.Student deleteStudent(
		it.hiree.register.model.Student student) {

		return getService().deleteStudent(student);
	}

	public static void deleteUniversityStudent(
		long universityId, long studentId) {

		getService().deleteUniversityStudent(universityId, studentId);
	}

	public static void deleteUniversityStudent(
		long universityId, it.hiree.register.model.Student student) {

		getService().deleteUniversityStudent(universityId, student);
	}

	public static void deleteUniversityStudents(
		long universityId,
		java.util.List<it.hiree.register.model.Student> students) {

		getService().deleteUniversityStudents(universityId, students);
	}

	public static void deleteUniversityStudents(
		long universityId, long[] studentIds) {

		getService().deleteUniversityStudents(universityId, studentIds);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.hiree.register.model.Student fetchStudent(long studentId) {
		return getService().fetchStudent(studentId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the student with the primary key.
	 *
	 * @param studentId the primary key of the student
	 * @return the student
	 * @throws PortalException if a student with the primary key could not be found
	 */
	public static it.hiree.register.model.Student getStudent(long studentId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getStudent(studentId);
	}

	/**
	 * Returns a range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of students
	 */
	public static java.util.List<it.hiree.register.model.Student> getStudents(
		int start, int end) {

		return getService().getStudents(start, end);
	}

	/**
	 * Returns the number of students.
	 *
	 * @return the number of students
	 */
	public static int getStudentsCount() {
		return getService().getStudentsCount();
	}

	/**
	 * Returns the universityIds of the universities associated with the student.
	 *
	 * @param studentId the studentId of the student
	 * @return long[] the universityIds of universities associated with the student
	 */
	public static long[] getUniversityPrimaryKeys(long studentId) {
		return getService().getUniversityPrimaryKeys(studentId);
	}

	public static java.util.List<it.hiree.register.model.Student>
		getUniversityStudents(long universityId) {

		return getService().getUniversityStudents(universityId);
	}

	public static java.util.List<it.hiree.register.model.Student>
		getUniversityStudents(long universityId, int start, int end) {

		return getService().getUniversityStudents(universityId, start, end);
	}

	public static java.util.List<it.hiree.register.model.Student>
		getUniversityStudents(
			long universityId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.hiree.register.model.Student> orderByComparator) {

		return getService().getUniversityStudents(
			universityId, start, end, orderByComparator);
	}

	public static int getUniversityStudentsCount(long universityId) {
		return getService().getUniversityStudentsCount(universityId);
	}

	public static boolean hasUniversityStudent(
		long universityId, long studentId) {

		return getService().hasUniversityStudent(universityId, studentId);
	}

	public static boolean hasUniversityStudents(long universityId) {
		return getService().hasUniversityStudents(universityId);
	}

	public static void setUniversityStudents(
		long universityId, long[] studentIds) {

		getService().setUniversityStudents(universityId, studentIds);
	}

	/**
	 * Updates the student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param student the student
	 * @return the student that was updated
	 */
	public static it.hiree.register.model.Student updateStudent(
		it.hiree.register.model.Student student) {

		return getService().updateStudent(student);
	}

	public static StudentLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<StudentLocalService, StudentLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(StudentLocalService.class);

		ServiceTracker<StudentLocalService, StudentLocalService>
			serviceTracker =
				new ServiceTracker<StudentLocalService, StudentLocalService>(
					bundle.getBundleContext(), StudentLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}