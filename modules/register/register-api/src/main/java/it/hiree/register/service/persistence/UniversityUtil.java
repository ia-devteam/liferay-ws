/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.hiree.register.model.University;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the university service. This utility wraps <code>it.hiree.register.service.persistence.impl.UniversityPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UniversityPersistence
 * @generated
 */
@ProviderType
public class UniversityUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(University university) {
		getPersistence().clearCache(university);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, University> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<University> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<University> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<University> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<University> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static University update(University university) {
		return getPersistence().update(university);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static University update(
		University university, ServiceContext serviceContext) {

		return getPersistence().update(university, serviceContext);
	}

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public static University findByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().findByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByGroupId_UniversityCode(long,String)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	@Deprecated
	public static University fetchByGroupId_UniversityCode(
		long groupId, String universityCode, boolean useFinderCache) {

		return getPersistence().fetchByGroupId_UniversityCode(
			groupId, universityCode, useFinderCache);
	}

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	public static University fetchByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return getPersistence().fetchByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Removes the university where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the university that was removed
	 */
	public static University removeByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().removeByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Returns the number of universities where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching universities
	 */
	public static int countByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return getPersistence().countByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Returns all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the matching universities
	 */
	public static List<University> findByGroupId_DataType(
		long groupId, short dataType) {

		return getPersistence().findByGroupId_DataType(groupId, dataType);
	}

	/**
	 * Returns a range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of matching universities
	 */
	public static List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end) {

		return getPersistence().findByGroupId_DataType(
			groupId, dataType, start, end);
	}

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_DataType(long,short, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching universities
	 */
	@Deprecated
	public static List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId_DataType(
			groupId, dataType, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching universities
	 */
	public static List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator) {

		return getPersistence().findByGroupId_DataType(
			groupId, dataType, start, end, orderByComparator);
	}

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public static University findByGroupId_DataType_First(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().findByGroupId_DataType_First(
			groupId, dataType, orderByComparator);
	}

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university, or <code>null</code> if a matching university could not be found
	 */
	public static University fetchByGroupId_DataType_First(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator) {

		return getPersistence().fetchByGroupId_DataType_First(
			groupId, dataType, orderByComparator);
	}

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public static University findByGroupId_DataType_Last(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().findByGroupId_DataType_Last(
			groupId, dataType, orderByComparator);
	}

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university, or <code>null</code> if a matching university could not be found
	 */
	public static University fetchByGroupId_DataType_Last(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator) {

		return getPersistence().fetchByGroupId_DataType_Last(
			groupId, dataType, orderByComparator);
	}

	/**
	 * Returns the universities before and after the current university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param universityId the primary key of the current university
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public static University[] findByGroupId_DataType_PrevAndNext(
			long universityId, long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().findByGroupId_DataType_PrevAndNext(
			universityId, groupId, dataType, orderByComparator);
	}

	/**
	 * Removes all the universities where groupId = &#63; and dataType = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 */
	public static void removeByGroupId_DataType(long groupId, short dataType) {
		getPersistence().removeByGroupId_DataType(groupId, dataType);
	}

	/**
	 * Returns the number of universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the number of matching universities
	 */
	public static int countByGroupId_DataType(long groupId, short dataType) {
		return getPersistence().countByGroupId_DataType(groupId, dataType);
	}

	/**
	 * Caches the university in the entity cache if it is enabled.
	 *
	 * @param university the university
	 */
	public static void cacheResult(University university) {
		getPersistence().cacheResult(university);
	}

	/**
	 * Caches the universities in the entity cache if it is enabled.
	 *
	 * @param universities the universities
	 */
	public static void cacheResult(List<University> universities) {
		getPersistence().cacheResult(universities);
	}

	/**
	 * Creates a new university with the primary key. Does not add the university to the database.
	 *
	 * @param universityId the primary key for the new university
	 * @return the new university
	 */
	public static University create(long universityId) {
		return getPersistence().create(universityId);
	}

	/**
	 * Removes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param universityId the primary key of the university
	 * @return the university that was removed
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public static University remove(long universityId)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().remove(universityId);
	}

	public static University updateImpl(University university) {
		return getPersistence().updateImpl(university);
	}

	/**
	 * Returns the university with the primary key or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public static University findByPrimaryKey(long universityId)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getPersistence().findByPrimaryKey(universityId);
	}

	/**
	 * Returns the university with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university, or <code>null</code> if a university with the primary key could not be found
	 */
	public static University fetchByPrimaryKey(long universityId) {
		return getPersistence().fetchByPrimaryKey(universityId);
	}

	/**
	 * Returns all the universities.
	 *
	 * @return the universities
	 */
	public static List<University> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of universities
	 */
	public static List<University> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of universities
	 */
	@Deprecated
	public static List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities
	 */
	public static List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the universities from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of universities.
	 *
	 * @return the number of universities
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return long[] of the primaryKeys of students associated with the university
	 */
	public static long[] getStudentPrimaryKeys(long pk) {
		return getPersistence().getStudentPrimaryKeys(pk);
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the universities associated with the student
	 */
	public static List<University> getStudentUniversities(long pk) {
		return getPersistence().getStudentUniversities(pk);
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of universities associated with the student
	 */
	public static List<University> getStudentUniversities(
		long pk, int start, int end) {

		return getPersistence().getStudentUniversities(pk, start, end);
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities associated with the student
	 */
	public static List<University> getStudentUniversities(
		long pk, int start, int end,
		OrderByComparator<University> orderByComparator) {

		return getPersistence().getStudentUniversities(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the number of students associated with the university
	 */
	public static int getStudentsSize(long pk) {
		return getPersistence().getStudentsSize(pk);
	}

	/**
	 * Returns <code>true</code> if the student is associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 * @return <code>true</code> if the student is associated with the university; <code>false</code> otherwise
	 */
	public static boolean containsStudent(long pk, long studentPK) {
		return getPersistence().containsStudent(pk, studentPK);
	}

	/**
	 * Returns <code>true</code> if the university has any students associated with it.
	 *
	 * @param pk the primary key of the university to check for associations with students
	 * @return <code>true</code> if the university has any students associated with it; <code>false</code> otherwise
	 */
	public static boolean containsStudents(long pk) {
		return getPersistence().containsStudents(pk);
	}

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	public static void addStudent(long pk, long studentPK) {
		getPersistence().addStudent(pk, studentPK);
	}

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	public static void addStudent(
		long pk, it.hiree.register.model.Student student) {

		getPersistence().addStudent(pk, student);
	}

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	public static void addStudents(long pk, long[] studentPKs) {
		getPersistence().addStudents(pk, studentPKs);
	}

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	public static void addStudents(
		long pk, List<it.hiree.register.model.Student> students) {

		getPersistence().addStudents(pk, students);
	}

	/**
	 * Clears all associations between the university and its students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university to clear the associated students from
	 */
	public static void clearStudents(long pk) {
		getPersistence().clearStudents(pk);
	}

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	public static void removeStudent(long pk, long studentPK) {
		getPersistence().removeStudent(pk, studentPK);
	}

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	public static void removeStudent(
		long pk, it.hiree.register.model.Student student) {

		getPersistence().removeStudent(pk, student);
	}

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	public static void removeStudents(long pk, long[] studentPKs) {
		getPersistence().removeStudents(pk, studentPKs);
	}

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	public static void removeStudents(
		long pk, List<it.hiree.register.model.Student> students) {

		getPersistence().removeStudents(pk, students);
	}

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students to be associated with the university
	 */
	public static void setStudents(long pk, long[] studentPKs) {
		getPersistence().setStudents(pk, studentPKs);
	}

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students to be associated with the university
	 */
	public static void setStudents(
		long pk, List<it.hiree.register.model.Student> students) {

		getPersistence().setStudents(pk, students);
	}

	public static UniversityPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UniversityPersistence, UniversityPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(UniversityPersistence.class);

		ServiceTracker<UniversityPersistence, UniversityPersistence>
			serviceTracker =
				new ServiceTracker
					<UniversityPersistence, UniversityPersistence>(
						bundle.getBundleContext(), UniversityPersistence.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}