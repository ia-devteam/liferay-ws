/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.hiree.register.exception.NoSuchStudentException;
import it.hiree.register.model.Student;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StudentUtil
 * @generated
 */
@ProviderType
public interface StudentPersistence extends BasePersistence<Student> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentUtil} to access the student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching students
	 */
	public java.util.List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode);

	/**
	 * Returns a range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of matching students
	 */
	public java.util.List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end);

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_UniversityCode(long,String, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching students
	 */
	@Deprecated
	public java.util.List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator, boolean useFinderCache);

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching students
	 */
	public java.util.List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator);

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	public Student findByGroupId_UniversityCode_First(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException;

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student, or <code>null</code> if a matching student could not be found
	 */
	public Student fetchByGroupId_UniversityCode_First(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator);

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	public Student findByGroupId_UniversityCode_Last(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException;

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student, or <code>null</code> if a matching student could not be found
	 */
	public Student fetchByGroupId_UniversityCode_Last(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator);

	/**
	 * Returns the students before and after the current student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param studentId the primary key of the current student
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public Student[] findByGroupId_UniversityCode_PrevAndNext(
			long studentId, long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException;

	/**
	 * Removes all the students where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 */
	public void removeByGroupId_UniversityCode(
		long groupId, String universityCode);

	/**
	 * Returns the number of students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching students
	 */
	public int countByGroupId_UniversityCode(
		long groupId, String universityCode);

	/**
	 * Caches the student in the entity cache if it is enabled.
	 *
	 * @param student the student
	 */
	public void cacheResult(Student student);

	/**
	 * Caches the students in the entity cache if it is enabled.
	 *
	 * @param students the students
	 */
	public void cacheResult(java.util.List<Student> students);

	/**
	 * Creates a new student with the primary key. Does not add the student to the database.
	 *
	 * @param studentId the primary key for the new student
	 * @return the new student
	 */
	public Student create(long studentId);

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the student
	 * @return the student that was removed
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public Student remove(long studentId) throws NoSuchStudentException;

	public Student updateImpl(Student student);

	/**
	 * Returns the student with the primary key or throws a <code>NoSuchStudentException</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public Student findByPrimaryKey(long studentId)
		throws NoSuchStudentException;

	/**
	 * Returns the student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student, or <code>null</code> if a student with the primary key could not be found
	 */
	public Student fetchByPrimaryKey(long studentId);

	/**
	 * Returns all the students.
	 *
	 * @return the students
	 */
	public java.util.List<Student> findAll();

	/**
	 * Returns a range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of students
	 */
	public java.util.List<Student> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of students
	 */
	@Deprecated
	public java.util.List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students
	 */
	public java.util.List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator);

	/**
	 * Removes all the students from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of students.
	 *
	 * @return the number of students
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return long[] of the primaryKeys of universities associated with the student
	 */
	public long[] getUniversityPrimaryKeys(long pk);

	/**
	 * Returns all the student associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the students associated with the university
	 */
	public java.util.List<Student> getUniversityStudents(long pk);

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of students associated with the university
	 */
	public java.util.List<Student> getUniversityStudents(
		long pk, int start, int end);

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students associated with the university
	 */
	public java.util.List<Student> getUniversityStudents(
		long pk, int start, int end,
		OrderByComparator<Student> orderByComparator);

	/**
	 * Returns the number of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of universities associated with the student
	 */
	public int getUniversitiesSize(long pk);

	/**
	 * Returns <code>true</code> if the university is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 * @return <code>true</code> if the university is associated with the student; <code>false</code> otherwise
	 */
	public boolean containsUniversity(long pk, long universityPK);

	/**
	 * Returns <code>true</code> if the student has any universities associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with universities
	 * @return <code>true</code> if the student has any universities associated with it; <code>false</code> otherwise
	 */
	public boolean containsUniversities(long pk);

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	public void addUniversity(long pk, long universityPK);

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	public void addUniversity(
		long pk, it.hiree.register.model.University university);

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	public void addUniversities(long pk, long[] universityPKs);

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	public void addUniversities(
		long pk,
		java.util.List<it.hiree.register.model.University> universities);

	/**
	 * Clears all associations between the student and its universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student to clear the associated universities from
	 */
	public void clearUniversities(long pk);

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	public void removeUniversity(long pk, long universityPK);

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	public void removeUniversity(
		long pk, it.hiree.register.model.University university);

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	public void removeUniversities(long pk, long[] universityPKs);

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	public void removeUniversities(
		long pk,
		java.util.List<it.hiree.register.model.University> universities);

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities to be associated with the student
	 */
	public void setUniversities(long pk, long[] universityPKs);

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities to be associated with the student
	 */
	public void setUniversities(
		long pk,
		java.util.List<it.hiree.register.model.University> universities);

}