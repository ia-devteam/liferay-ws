/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link UniversityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UniversityLocalService
 * @generated
 */
@ProviderType
public class UniversityLocalServiceWrapper
	implements UniversityLocalService, ServiceWrapper<UniversityLocalService> {

	public UniversityLocalServiceWrapper(
		UniversityLocalService universityLocalService) {

		_universityLocalService = universityLocalService;
	}

	@Override
	public void addStudentUniversities(
		long studentId,
		java.util.List<it.hiree.register.model.University> universities) {

		_universityLocalService.addStudentUniversities(studentId, universities);
	}

	@Override
	public void addStudentUniversities(long studentId, long[] universityIds) {
		_universityLocalService.addStudentUniversities(
			studentId, universityIds);
	}

	@Override
	public void addStudentUniversity(long studentId, long universityId) {
		_universityLocalService.addStudentUniversity(studentId, universityId);
	}

	@Override
	public void addStudentUniversity(
		long studentId, it.hiree.register.model.University university) {

		_universityLocalService.addStudentUniversity(studentId, university);
	}

	/**
	 * Adds the university to the database. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was added
	 */
	@Override
	public it.hiree.register.model.University addUniversity(
		it.hiree.register.model.University university) {

		return _universityLocalService.addUniversity(university);
	}

	@Override
	public void clearStudentUniversities(long studentId) {
		_universityLocalService.clearStudentUniversities(studentId);
	}

	/**
	 * Creates a new university with the primary key. Does not add the university to the database.
	 *
	 * @param universityId the primary key for the new university
	 * @return the new university
	 */
	@Override
	public it.hiree.register.model.University createUniversity(
		long universityId) {

		return _universityLocalService.createUniversity(universityId);
	}

	@Override
	public it.hiree.register.model.University createUniversity(
		long userId, long groupId, long companyId, String name,
		String universityCode, String url, short dataType,
		com.liferay.portal.kernel.service.ServiceContext serviceContext) {

		return _universityLocalService.createUniversity(
			userId, groupId, companyId, name, universityCode, url, dataType,
			serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _universityLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public void deleteStudentUniversities(
		long studentId,
		java.util.List<it.hiree.register.model.University> universities) {

		_universityLocalService.deleteStudentUniversities(
			studentId, universities);
	}

	@Override
	public void deleteStudentUniversities(
		long studentId, long[] universityIds) {

		_universityLocalService.deleteStudentUniversities(
			studentId, universityIds);
	}

	@Override
	public void deleteStudentUniversity(long studentId, long universityId) {
		_universityLocalService.deleteStudentUniversity(
			studentId, universityId);
	}

	@Override
	public void deleteStudentUniversity(
		long studentId, it.hiree.register.model.University university) {

		_universityLocalService.deleteStudentUniversity(studentId, university);
	}

	/**
	 * Deletes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param universityId the primary key of the university
	 * @return the university that was removed
	 * @throws PortalException if a university with the primary key could not be found
	 */
	@Override
	public it.hiree.register.model.University deleteUniversity(
			long universityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _universityLocalService.deleteUniversity(universityId);
	}

	/**
	 * Deletes the university from the database. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was removed
	 */
	@Override
	public it.hiree.register.model.University deleteUniversity(
		it.hiree.register.model.University university) {

		return _universityLocalService.deleteUniversity(university);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _universityLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _universityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _universityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _universityLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _universityLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _universityLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public it.hiree.register.model.University fetchByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return _universityLocalService.fetchByGroupId_UniversityCode(
			groupId, universityCode);
	}

	@Override
	public it.hiree.register.model.University fetchUniversity(
		long universityId) {

		return _universityLocalService.fetchUniversity(universityId);
	}

	@Override
	public java.util.List<it.hiree.register.model.University> findAll(
		int start, int end) {

		return _universityLocalService.findAll(start, end);
	}

	@Override
	public java.util.List<it.hiree.register.model.University>
		findByGroupId_DataType(long groupId, short dataType) {

		return _universityLocalService.findByGroupId_DataType(
			groupId, dataType);
	}

	@Override
	public it.hiree.register.model.University findByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return _universityLocalService.findByGroupId_UniversityCode(
			groupId, universityCode);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _universityLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _universityLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _universityLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _universityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the studentIds of the students associated with the university.
	 *
	 * @param universityId the universityId of the university
	 * @return long[] the studentIds of students associated with the university
	 */
	@Override
	public long[] getStudentPrimaryKeys(long universityId) {
		return _universityLocalService.getStudentPrimaryKeys(universityId);
	}

	@Override
	public java.util.List<it.hiree.register.model.University>
		getStudentUniversities(long studentId) {

		return _universityLocalService.getStudentUniversities(studentId);
	}

	@Override
	public java.util.List<it.hiree.register.model.University>
		getStudentUniversities(long studentId, int start, int end) {

		return _universityLocalService.getStudentUniversities(
			studentId, start, end);
	}

	@Override
	public java.util.List<it.hiree.register.model.University>
		getStudentUniversities(
			long studentId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.hiree.register.model.University> orderByComparator) {

		return _universityLocalService.getStudentUniversities(
			studentId, start, end, orderByComparator);
	}

	@Override
	public int getStudentUniversitiesCount(long studentId) {
		return _universityLocalService.getStudentUniversitiesCount(studentId);
	}

	/**
	 * Returns a range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of universities
	 */
	@Override
	public java.util.List<it.hiree.register.model.University> getUniversities(
		int start, int end) {

		return _universityLocalService.getUniversities(start, end);
	}

	/**
	 * Returns the number of universities.
	 *
	 * @return the number of universities
	 */
	@Override
	public int getUniversitiesCount() {
		return _universityLocalService.getUniversitiesCount();
	}

	/**
	 * Returns the university with the primary key.
	 *
	 * @param universityId the primary key of the university
	 * @return the university
	 * @throws PortalException if a university with the primary key could not be found
	 */
	@Override
	public it.hiree.register.model.University getUniversity(long universityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _universityLocalService.getUniversity(universityId);
	}

	@Override
	public boolean hasStudentUniversities(long studentId) {
		return _universityLocalService.hasStudentUniversities(studentId);
	}

	@Override
	public boolean hasStudentUniversity(long studentId, long universityId) {
		return _universityLocalService.hasStudentUniversity(
			studentId, universityId);
	}

	@Override
	public void setStudentUniversities(long studentId, long[] universityIds) {
		_universityLocalService.setStudentUniversities(
			studentId, universityIds);
	}

	@Override
	public it.hiree.register.model.University updateUniversity(
			long userId, long groupId, long companyId, long universityId,
			String name, String universityCode, String url, short dataType,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return _universityLocalService.updateUniversity(
			userId, groupId, companyId, universityId, name, universityCode, url,
			dataType, serviceContext);
	}

	/**
	 * Updates the university in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was updated
	 */
	@Override
	public it.hiree.register.model.University updateUniversity(
		it.hiree.register.model.University university) {

		return _universityLocalService.updateUniversity(university);
	}

	@Override
	public UniversityLocalService getWrappedService() {
		return _universityLocalService;
	}

	@Override
	public void setWrappedService(
		UniversityLocalService universityLocalService) {

		_universityLocalService = universityLocalService;
	}

	private UniversityLocalService _universityLocalService;

}