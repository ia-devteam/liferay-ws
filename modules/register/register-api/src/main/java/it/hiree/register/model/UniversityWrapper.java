/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link University}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see University
 * @generated
 */
@ProviderType
public class UniversityWrapper
	extends BaseModelWrapper<University>
	implements University, ModelWrapper<University> {

	public UniversityWrapper(University university) {
		super(university);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("universityId", getUniversityId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("name", getName());
		attributes.put("universityCode", getUniversityCode());
		attributes.put("url", getUrl());
		attributes.put("dataType", getDataType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long universityId = (Long)attributes.get("universityId");

		if (universityId != null) {
			setUniversityId(universityId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String universityCode = (String)attributes.get("universityCode");

		if (universityCode != null) {
			setUniversityCode(universityCode);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		Short dataType = (Short)attributes.get("dataType");

		if (dataType != null) {
			setDataType(dataType);
		}
	}

	/**
	 * Returns the company ID of this university.
	 *
	 * @return the company ID of this university
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this university.
	 *
	 * @return the create date of this university
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the data type of this university.
	 *
	 * @return the data type of this university
	 */
	@Override
	public short getDataType() {
		return model.getDataType();
	}

	/**
	 * Returns the group ID of this university.
	 *
	 * @return the group ID of this university
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this university.
	 *
	 * @return the modified date of this university
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this university.
	 *
	 * @return the name of this university
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the primary key of this university.
	 *
	 * @return the primary key of this university
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the university code of this university.
	 *
	 * @return the university code of this university
	 */
	@Override
	public String getUniversityCode() {
		return model.getUniversityCode();
	}

	/**
	 * Returns the university ID of this university.
	 *
	 * @return the university ID of this university
	 */
	@Override
	public long getUniversityId() {
		return model.getUniversityId();
	}

	/**
	 * Returns the url of this university.
	 *
	 * @return the url of this university
	 */
	@Override
	public String getUrl() {
		return model.getUrl();
	}

	/**
	 * Returns the user ID of this university.
	 *
	 * @return the user ID of this university
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this university.
	 *
	 * @return the user name of this university
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this university.
	 *
	 * @return the user uuid of this university
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this university.
	 *
	 * @param companyId the company ID of this university
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this university.
	 *
	 * @param createDate the create date of this university
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the data type of this university.
	 *
	 * @param dataType the data type of this university
	 */
	@Override
	public void setDataType(short dataType) {
		model.setDataType(dataType);
	}

	/**
	 * Sets the group ID of this university.
	 *
	 * @param groupId the group ID of this university
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this university.
	 *
	 * @param modifiedDate the modified date of this university
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this university.
	 *
	 * @param name the name of this university
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the primary key of this university.
	 *
	 * @param primaryKey the primary key of this university
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the university code of this university.
	 *
	 * @param universityCode the university code of this university
	 */
	@Override
	public void setUniversityCode(String universityCode) {
		model.setUniversityCode(universityCode);
	}

	/**
	 * Sets the university ID of this university.
	 *
	 * @param universityId the university ID of this university
	 */
	@Override
	public void setUniversityId(long universityId) {
		model.setUniversityId(universityId);
	}

	/**
	 * Sets the url of this university.
	 *
	 * @param url the url of this university
	 */
	@Override
	public void setUrl(String url) {
		model.setUrl(url);
	}

	/**
	 * Sets the user ID of this university.
	 *
	 * @param userId the user ID of this university
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this university.
	 *
	 * @param userName the user name of this university
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this university.
	 *
	 * @param userUuid the user uuid of this university
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	@Override
	protected UniversityWrapper wrap(University university) {
		return new UniversityWrapper(university);
	}

}