/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.hiree.register.model.Student;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the student service. This utility wraps <code>it.hiree.register.service.persistence.impl.StudentPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StudentPersistence
 * @generated
 */
@ProviderType
public class StudentUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Student student) {
		getPersistence().clearCache(student);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Student> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Student> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Student> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Student> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Student> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Student update(Student student) {
		return getPersistence().update(student);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Student update(
		Student student, ServiceContext serviceContext) {

		return getPersistence().update(student, serviceContext);
	}

	/**
	 * Returns all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching students
	 */
	public static List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return getPersistence().findByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Returns a range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of matching students
	 */
	public static List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end) {

		return getPersistence().findByGroupId_UniversityCode(
			groupId, universityCode, start, end);
	}

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_UniversityCode(long,String, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching students
	 */
	@Deprecated
	public static List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId_UniversityCode(
			groupId, universityCode, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching students
	 */
	public static List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator) {

		return getPersistence().findByGroupId_UniversityCode(
			groupId, universityCode, start, end, orderByComparator);
	}

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	public static Student findByGroupId_UniversityCode_First(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws it.hiree.register.exception.NoSuchStudentException {

		return getPersistence().findByGroupId_UniversityCode_First(
			groupId, universityCode, orderByComparator);
	}

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student, or <code>null</code> if a matching student could not be found
	 */
	public static Student fetchByGroupId_UniversityCode_First(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator) {

		return getPersistence().fetchByGroupId_UniversityCode_First(
			groupId, universityCode, orderByComparator);
	}

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	public static Student findByGroupId_UniversityCode_Last(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws it.hiree.register.exception.NoSuchStudentException {

		return getPersistence().findByGroupId_UniversityCode_Last(
			groupId, universityCode, orderByComparator);
	}

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student, or <code>null</code> if a matching student could not be found
	 */
	public static Student fetchByGroupId_UniversityCode_Last(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator) {

		return getPersistence().fetchByGroupId_UniversityCode_Last(
			groupId, universityCode, orderByComparator);
	}

	/**
	 * Returns the students before and after the current student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param studentId the primary key of the current student
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public static Student[] findByGroupId_UniversityCode_PrevAndNext(
			long studentId, long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws it.hiree.register.exception.NoSuchStudentException {

		return getPersistence().findByGroupId_UniversityCode_PrevAndNext(
			studentId, groupId, universityCode, orderByComparator);
	}

	/**
	 * Removes all the students where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 */
	public static void removeByGroupId_UniversityCode(
		long groupId, String universityCode) {

		getPersistence().removeByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Returns the number of students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching students
	 */
	public static int countByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return getPersistence().countByGroupId_UniversityCode(
			groupId, universityCode);
	}

	/**
	 * Caches the student in the entity cache if it is enabled.
	 *
	 * @param student the student
	 */
	public static void cacheResult(Student student) {
		getPersistence().cacheResult(student);
	}

	/**
	 * Caches the students in the entity cache if it is enabled.
	 *
	 * @param students the students
	 */
	public static void cacheResult(List<Student> students) {
		getPersistence().cacheResult(students);
	}

	/**
	 * Creates a new student with the primary key. Does not add the student to the database.
	 *
	 * @param studentId the primary key for the new student
	 * @return the new student
	 */
	public static Student create(long studentId) {
		return getPersistence().create(studentId);
	}

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the student
	 * @return the student that was removed
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public static Student remove(long studentId)
		throws it.hiree.register.exception.NoSuchStudentException {

		return getPersistence().remove(studentId);
	}

	public static Student updateImpl(Student student) {
		return getPersistence().updateImpl(student);
	}

	/**
	 * Returns the student with the primary key or throws a <code>NoSuchStudentException</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	public static Student findByPrimaryKey(long studentId)
		throws it.hiree.register.exception.NoSuchStudentException {

		return getPersistence().findByPrimaryKey(studentId);
	}

	/**
	 * Returns the student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student, or <code>null</code> if a student with the primary key could not be found
	 */
	public static Student fetchByPrimaryKey(long studentId) {
		return getPersistence().fetchByPrimaryKey(studentId);
	}

	/**
	 * Returns all the students.
	 *
	 * @return the students
	 */
	public static List<Student> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of students
	 */
	public static List<Student> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of students
	 */
	@Deprecated
	public static List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students
	 */
	public static List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the students from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of students.
	 *
	 * @return the number of students
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return long[] of the primaryKeys of universities associated with the student
	 */
	public static long[] getUniversityPrimaryKeys(long pk) {
		return getPersistence().getUniversityPrimaryKeys(pk);
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the students associated with the university
	 */
	public static List<Student> getUniversityStudents(long pk) {
		return getPersistence().getUniversityStudents(pk);
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of students associated with the university
	 */
	public static List<Student> getUniversityStudents(
		long pk, int start, int end) {

		return getPersistence().getUniversityStudents(pk, start, end);
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students associated with the university
	 */
	public static List<Student> getUniversityStudents(
		long pk, int start, int end,
		OrderByComparator<Student> orderByComparator) {

		return getPersistence().getUniversityStudents(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of universities associated with the student
	 */
	public static int getUniversitiesSize(long pk) {
		return getPersistence().getUniversitiesSize(pk);
	}

	/**
	 * Returns <code>true</code> if the university is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 * @return <code>true</code> if the university is associated with the student; <code>false</code> otherwise
	 */
	public static boolean containsUniversity(long pk, long universityPK) {
		return getPersistence().containsUniversity(pk, universityPK);
	}

	/**
	 * Returns <code>true</code> if the student has any universities associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with universities
	 * @return <code>true</code> if the student has any universities associated with it; <code>false</code> otherwise
	 */
	public static boolean containsUniversities(long pk) {
		return getPersistence().containsUniversities(pk);
	}

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	public static void addUniversity(long pk, long universityPK) {
		getPersistence().addUniversity(pk, universityPK);
	}

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	public static void addUniversity(
		long pk, it.hiree.register.model.University university) {

		getPersistence().addUniversity(pk, university);
	}

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	public static void addUniversities(long pk, long[] universityPKs) {
		getPersistence().addUniversities(pk, universityPKs);
	}

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	public static void addUniversities(
		long pk, List<it.hiree.register.model.University> universities) {

		getPersistence().addUniversities(pk, universities);
	}

	/**
	 * Clears all associations between the student and its universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student to clear the associated universities from
	 */
	public static void clearUniversities(long pk) {
		getPersistence().clearUniversities(pk);
	}

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	public static void removeUniversity(long pk, long universityPK) {
		getPersistence().removeUniversity(pk, universityPK);
	}

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	public static void removeUniversity(
		long pk, it.hiree.register.model.University university) {

		getPersistence().removeUniversity(pk, university);
	}

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	public static void removeUniversities(long pk, long[] universityPKs) {
		getPersistence().removeUniversities(pk, universityPKs);
	}

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	public static void removeUniversities(
		long pk, List<it.hiree.register.model.University> universities) {

		getPersistence().removeUniversities(pk, universities);
	}

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities to be associated with the student
	 */
	public static void setUniversities(long pk, long[] universityPKs) {
		getPersistence().setUniversities(pk, universityPKs);
	}

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities to be associated with the student
	 */
	public static void setUniversities(
		long pk, List<it.hiree.register.model.University> universities) {

		getPersistence().setUniversities(pk, universities);
	}

	public static StudentPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<StudentPersistence, StudentPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(StudentPersistence.class);

		ServiceTracker<StudentPersistence, StudentPersistence> serviceTracker =
			new ServiceTracker<StudentPersistence, StudentPersistence>(
				bundle.getBundleContext(), StudentPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}