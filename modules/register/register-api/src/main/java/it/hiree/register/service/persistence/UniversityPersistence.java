/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.hiree.register.exception.NoSuchUniversityException;
import it.hiree.register.model.University;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the university service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UniversityUtil
 * @generated
 */
@ProviderType
public interface UniversityPersistence extends BasePersistence<University> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UniversityUtil} to access the university persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public University findByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws NoSuchUniversityException;

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByGroupId_UniversityCode(long,String)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	@Deprecated
	public University fetchByGroupId_UniversityCode(
		long groupId, String universityCode, boolean useFinderCache);

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	public University fetchByGroupId_UniversityCode(
		long groupId, String universityCode);

	/**
	 * Removes the university where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the university that was removed
	 */
	public University removeByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws NoSuchUniversityException;

	/**
	 * Returns the number of universities where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching universities
	 */
	public int countByGroupId_UniversityCode(
		long groupId, String universityCode);

	/**
	 * Returns all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the matching universities
	 */
	public java.util.List<University> findByGroupId_DataType(
		long groupId, short dataType);

	/**
	 * Returns a range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of matching universities
	 */
	public java.util.List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end);

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_DataType(long,short, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching universities
	 */
	@Deprecated
	public java.util.List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching universities
	 */
	public java.util.List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator);

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public University findByGroupId_DataType_First(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException;

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university, or <code>null</code> if a matching university could not be found
	 */
	public University fetchByGroupId_DataType_First(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator);

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	public University findByGroupId_DataType_Last(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException;

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university, or <code>null</code> if a matching university could not be found
	 */
	public University fetchByGroupId_DataType_Last(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator);

	/**
	 * Returns the universities before and after the current university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param universityId the primary key of the current university
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public University[] findByGroupId_DataType_PrevAndNext(
			long universityId, long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException;

	/**
	 * Removes all the universities where groupId = &#63; and dataType = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 */
	public void removeByGroupId_DataType(long groupId, short dataType);

	/**
	 * Returns the number of universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the number of matching universities
	 */
	public int countByGroupId_DataType(long groupId, short dataType);

	/**
	 * Caches the university in the entity cache if it is enabled.
	 *
	 * @param university the university
	 */
	public void cacheResult(University university);

	/**
	 * Caches the universities in the entity cache if it is enabled.
	 *
	 * @param universities the universities
	 */
	public void cacheResult(java.util.List<University> universities);

	/**
	 * Creates a new university with the primary key. Does not add the university to the database.
	 *
	 * @param universityId the primary key for the new university
	 * @return the new university
	 */
	public University create(long universityId);

	/**
	 * Removes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param universityId the primary key of the university
	 * @return the university that was removed
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public University remove(long universityId)
		throws NoSuchUniversityException;

	public University updateImpl(University university);

	/**
	 * Returns the university with the primary key or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	public University findByPrimaryKey(long universityId)
		throws NoSuchUniversityException;

	/**
	 * Returns the university with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university, or <code>null</code> if a university with the primary key could not be found
	 */
	public University fetchByPrimaryKey(long universityId);

	/**
	 * Returns all the universities.
	 *
	 * @return the universities
	 */
	public java.util.List<University> findAll();

	/**
	 * Returns a range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of universities
	 */
	public java.util.List<University> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of universities
	 */
	@Deprecated
	public java.util.List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities
	 */
	public java.util.List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator);

	/**
	 * Removes all the universities from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of universities.
	 *
	 * @return the number of universities
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return long[] of the primaryKeys of students associated with the university
	 */
	public long[] getStudentPrimaryKeys(long pk);

	/**
	 * Returns all the university associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the universities associated with the student
	 */
	public java.util.List<University> getStudentUniversities(long pk);

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of universities associated with the student
	 */
	public java.util.List<University> getStudentUniversities(
		long pk, int start, int end);

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities associated with the student
	 */
	public java.util.List<University> getStudentUniversities(
		long pk, int start, int end,
		OrderByComparator<University> orderByComparator);

	/**
	 * Returns the number of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the number of students associated with the university
	 */
	public int getStudentsSize(long pk);

	/**
	 * Returns <code>true</code> if the student is associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 * @return <code>true</code> if the student is associated with the university; <code>false</code> otherwise
	 */
	public boolean containsStudent(long pk, long studentPK);

	/**
	 * Returns <code>true</code> if the university has any students associated with it.
	 *
	 * @param pk the primary key of the university to check for associations with students
	 * @return <code>true</code> if the university has any students associated with it; <code>false</code> otherwise
	 */
	public boolean containsStudents(long pk);

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	public void addStudent(long pk, long studentPK);

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	public void addStudent(long pk, it.hiree.register.model.Student student);

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	public void addStudents(long pk, long[] studentPKs);

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	public void addStudents(
		long pk, java.util.List<it.hiree.register.model.Student> students);

	/**
	 * Clears all associations between the university and its students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university to clear the associated students from
	 */
	public void clearStudents(long pk);

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	public void removeStudent(long pk, long studentPK);

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	public void removeStudent(long pk, it.hiree.register.model.Student student);

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	public void removeStudents(long pk, long[] studentPKs);

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	public void removeStudents(
		long pk, java.util.List<it.hiree.register.model.Student> students);

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students to be associated with the university
	 */
	public void setStudents(long pk, long[] studentPKs);

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students to be associated with the university
	 */
	public void setStudents(
		long pk, java.util.List<it.hiree.register.model.Student> students);

}