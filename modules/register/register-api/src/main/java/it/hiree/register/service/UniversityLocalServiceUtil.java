/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for University. This utility wraps
 * <code>it.hiree.register.service.impl.UniversityLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see UniversityLocalService
 * @generated
 */
@ProviderType
public class UniversityLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.hiree.register.service.impl.UniversityLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static void addStudentUniversities(
		long studentId,
		java.util.List<it.hiree.register.model.University> universities) {

		getService().addStudentUniversities(studentId, universities);
	}

	public static void addStudentUniversities(
		long studentId, long[] universityIds) {

		getService().addStudentUniversities(studentId, universityIds);
	}

	public static void addStudentUniversity(long studentId, long universityId) {
		getService().addStudentUniversity(studentId, universityId);
	}

	public static void addStudentUniversity(
		long studentId, it.hiree.register.model.University university) {

		getService().addStudentUniversity(studentId, university);
	}

	/**
	 * Adds the university to the database. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was added
	 */
	public static it.hiree.register.model.University addUniversity(
		it.hiree.register.model.University university) {

		return getService().addUniversity(university);
	}

	public static void clearStudentUniversities(long studentId) {
		getService().clearStudentUniversities(studentId);
	}

	/**
	 * Creates a new university with the primary key. Does not add the university to the database.
	 *
	 * @param universityId the primary key for the new university
	 * @return the new university
	 */
	public static it.hiree.register.model.University createUniversity(
		long universityId) {

		return getService().createUniversity(universityId);
	}

	public static it.hiree.register.model.University createUniversity(
		long userId, long groupId, long companyId, String name,
		String universityCode, String url, short dataType,
		com.liferay.portal.kernel.service.ServiceContext serviceContext) {

		return getService().createUniversity(
			userId, groupId, companyId, name, universityCode, url, dataType,
			serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static void deleteStudentUniversities(
		long studentId,
		java.util.List<it.hiree.register.model.University> universities) {

		getService().deleteStudentUniversities(studentId, universities);
	}

	public static void deleteStudentUniversities(
		long studentId, long[] universityIds) {

		getService().deleteStudentUniversities(studentId, universityIds);
	}

	public static void deleteStudentUniversity(
		long studentId, long universityId) {

		getService().deleteStudentUniversity(studentId, universityId);
	}

	public static void deleteStudentUniversity(
		long studentId, it.hiree.register.model.University university) {

		getService().deleteStudentUniversity(studentId, university);
	}

	/**
	 * Deletes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param universityId the primary key of the university
	 * @return the university that was removed
	 * @throws PortalException if a university with the primary key could not be found
	 */
	public static it.hiree.register.model.University deleteUniversity(
			long universityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteUniversity(universityId);
	}

	/**
	 * Deletes the university from the database. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was removed
	 */
	public static it.hiree.register.model.University deleteUniversity(
		it.hiree.register.model.University university) {

		return getService().deleteUniversity(university);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.hiree.register.model.University
		fetchByGroupId_UniversityCode(long groupId, String universityCode) {

		return getService().fetchByGroupId_UniversityCode(
			groupId, universityCode);
	}

	public static it.hiree.register.model.University fetchUniversity(
		long universityId) {

		return getService().fetchUniversity(universityId);
	}

	public static java.util.List<it.hiree.register.model.University> findAll(
		int start, int end) {

		return getService().findAll(start, end);
	}

	public static java.util.List<it.hiree.register.model.University>
		findByGroupId_DataType(long groupId, short dataType) {

		return getService().findByGroupId_DataType(groupId, dataType);
	}

	public static it.hiree.register.model.University
			findByGroupId_UniversityCode(long groupId, String universityCode)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getService().findByGroupId_UniversityCode(
			groupId, universityCode);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the studentIds of the students associated with the university.
	 *
	 * @param universityId the universityId of the university
	 * @return long[] the studentIds of students associated with the university
	 */
	public static long[] getStudentPrimaryKeys(long universityId) {
		return getService().getStudentPrimaryKeys(universityId);
	}

	public static java.util.List<it.hiree.register.model.University>
		getStudentUniversities(long studentId) {

		return getService().getStudentUniversities(studentId);
	}

	public static java.util.List<it.hiree.register.model.University>
		getStudentUniversities(long studentId, int start, int end) {

		return getService().getStudentUniversities(studentId, start, end);
	}

	public static java.util.List<it.hiree.register.model.University>
		getStudentUniversities(
			long studentId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.hiree.register.model.University> orderByComparator) {

		return getService().getStudentUniversities(
			studentId, start, end, orderByComparator);
	}

	public static int getStudentUniversitiesCount(long studentId) {
		return getService().getStudentUniversitiesCount(studentId);
	}

	/**
	 * Returns a range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.hiree.register.model.impl.UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of universities
	 */
	public static java.util.List<it.hiree.register.model.University>
		getUniversities(int start, int end) {

		return getService().getUniversities(start, end);
	}

	/**
	 * Returns the number of universities.
	 *
	 * @return the number of universities
	 */
	public static int getUniversitiesCount() {
		return getService().getUniversitiesCount();
	}

	/**
	 * Returns the university with the primary key.
	 *
	 * @param universityId the primary key of the university
	 * @return the university
	 * @throws PortalException if a university with the primary key could not be found
	 */
	public static it.hiree.register.model.University getUniversity(
			long universityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getUniversity(universityId);
	}

	public static boolean hasStudentUniversities(long studentId) {
		return getService().hasStudentUniversities(studentId);
	}

	public static boolean hasStudentUniversity(
		long studentId, long universityId) {

		return getService().hasStudentUniversity(studentId, universityId);
	}

	public static void setStudentUniversities(
		long studentId, long[] universityIds) {

		getService().setStudentUniversities(studentId, universityIds);
	}

	public static it.hiree.register.model.University updateUniversity(
			long userId, long groupId, long companyId, long universityId,
			String name, String universityCode, String url, short dataType,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws it.hiree.register.exception.NoSuchUniversityException {

		return getService().updateUniversity(
			userId, groupId, companyId, universityId, name, universityCode, url,
			dataType, serviceContext);
	}

	/**
	 * Updates the university in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param university the university
	 * @return the university that was updated
	 */
	public static it.hiree.register.model.University updateUniversity(
		it.hiree.register.model.University university) {

		return getService().updateUniversity(university);
	}

	public static UniversityLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<UniversityLocalService, UniversityLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(UniversityLocalService.class);

		ServiceTracker<UniversityLocalService, UniversityLocalService>
			serviceTracker =
				new ServiceTracker
					<UniversityLocalService, UniversityLocalService>(
						bundle.getBundleContext(), UniversityLocalService.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}