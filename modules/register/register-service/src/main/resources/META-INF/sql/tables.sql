create table hiree_Student (
	studentId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	surName VARCHAR(75) null,
	universityCode VARCHAR(75) null,
	mappingUserId LONG,
	course VARCHAR(75) null,
	finished BOOLEAN
);

create table hiree_Student_University (
	companyId LONG not null,
	studentId LONG not null,
	universityId LONG not null,
	primary key (studentId, universityId)
);

create table hiree_University (
	universityId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	universityCode VARCHAR(75) null,
	url VARCHAR(75) null,
	dataType INTEGER
);