create index IX_57B02C06 on hiree_Student (groupId, universityCode[$COLUMN_LENGTH:75$]);

create index IX_CC89FA42 on hiree_Student_University (companyId);
create index IX_2DB91FA3 on hiree_Student_University (universityId);

create index IX_92704C60 on hiree_University (groupId, dataType);
create index IX_EB3D02B7 on hiree_University (groupId, universityCode[$COLUMN_LENGTH:75$]);