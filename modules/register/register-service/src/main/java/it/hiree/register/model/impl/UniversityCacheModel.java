/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import it.hiree.register.model.University;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing University in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class UniversityCacheModel
	implements CacheModel<University>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UniversityCacheModel)) {
			return false;
		}

		UniversityCacheModel universityCacheModel = (UniversityCacheModel)obj;

		if (universityId == universityCacheModel.universityId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, universityId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{universityId=");
		sb.append(universityId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", name=");
		sb.append(name);
		sb.append(", universityCode=");
		sb.append(universityCode);
		sb.append(", url=");
		sb.append(url);
		sb.append(", dataType=");
		sb.append(dataType);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public University toEntityModel() {
		UniversityImpl universityImpl = new UniversityImpl();

		universityImpl.setUniversityId(universityId);
		universityImpl.setGroupId(groupId);
		universityImpl.setCompanyId(companyId);
		universityImpl.setUserId(userId);

		if (userName == null) {
			universityImpl.setUserName("");
		}
		else {
			universityImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			universityImpl.setCreateDate(null);
		}
		else {
			universityImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			universityImpl.setModifiedDate(null);
		}
		else {
			universityImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (name == null) {
			universityImpl.setName("");
		}
		else {
			universityImpl.setName(name);
		}

		if (universityCode == null) {
			universityImpl.setUniversityCode("");
		}
		else {
			universityImpl.setUniversityCode(universityCode);
		}

		if (url == null) {
			universityImpl.setUrl("");
		}
		else {
			universityImpl.setUrl(url);
		}

		universityImpl.setDataType(dataType);

		universityImpl.resetOriginalValues();

		return universityImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		universityId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		name = objectInput.readUTF();
		universityCode = objectInput.readUTF();
		url = objectInput.readUTF();

		dataType = objectInput.readShort();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(universityId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (universityCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(universityCode);
		}

		if (url == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(url);
		}

		objectOutput.writeShort(dataType);
	}

	public long universityId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String name;
	public String universityCode;
	public String url;
	public short dataType;

}