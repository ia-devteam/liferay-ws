/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;

import it.hiree.register.exception.NoSuchUniversityException;
import it.hiree.register.model.Student;
import it.hiree.register.model.University;
import it.hiree.register.model.impl.UniversityImpl;
import it.hiree.register.model.impl.UniversityModelImpl;
import it.hiree.register.service.persistence.UniversityPersistence;
import it.hiree.register.service.persistence.impl.constants.hireePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the university service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = UniversityPersistence.class)
@ProviderType
public class UniversityPersistenceImpl
	extends BasePersistenceImpl<University> implements UniversityPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>UniversityUtil</code> to access the university persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		UniversityImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathFetchByGroupId_UniversityCode;
	private FinderPath _finderPathCountByGroupId_UniversityCode;

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	@Override
	public University findByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws NoSuchUniversityException {

		University university = fetchByGroupId_UniversityCode(
			groupId, universityCode);

		if (university == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(", universityCode=");
			msg.append(universityCode);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchUniversityException(msg.toString());
		}

		return university;
	}

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByGroupId_UniversityCode(long,String)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	@Deprecated
	@Override
	public University fetchByGroupId_UniversityCode(
		long groupId, String universityCode, boolean useFinderCache) {

		return fetchByGroupId_UniversityCode(groupId, universityCode);
	}

	/**
	 * Returns the university where groupId = &#63; and universityCode = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching university, or <code>null</code> if a matching university could not be found
	 */
	@Override
	public University fetchByGroupId_UniversityCode(
		long groupId, String universityCode) {

		universityCode = Objects.toString(universityCode, "");

		Object[] finderArgs = new Object[] {groupId, universityCode};

		Object result = finderCache.getResult(
			_finderPathFetchByGroupId_UniversityCode, finderArgs, this);

		if (result instanceof University) {
			University university = (University)result;

			if ((groupId != university.getGroupId()) ||
				!Objects.equals(
					universityCode, university.getUniversityCode())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_UNIVERSITY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2);

			boolean bindUniversityCode = false;

			if (universityCode.isEmpty()) {
				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3);
			}
			else {
				bindUniversityCode = true;

				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindUniversityCode) {
					qPos.add(universityCode);
				}

				List<University> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByGroupId_UniversityCode, finderArgs,
						list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"UniversityPersistenceImpl.fetchByGroupId_UniversityCode(long, String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					University university = list.get(0);

					result = university;

					cacheResult(university);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathFetchByGroupId_UniversityCode, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (University)result;
		}
	}

	/**
	 * Removes the university where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the university that was removed
	 */
	@Override
	public University removeByGroupId_UniversityCode(
			long groupId, String universityCode)
		throws NoSuchUniversityException {

		University university = findByGroupId_UniversityCode(
			groupId, universityCode);

		return remove(university);
	}

	/**
	 * Returns the number of universities where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching universities
	 */
	@Override
	public int countByGroupId_UniversityCode(
		long groupId, String universityCode) {

		universityCode = Objects.toString(universityCode, "");

		FinderPath finderPath = _finderPathCountByGroupId_UniversityCode;

		Object[] finderArgs = new Object[] {groupId, universityCode};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_UNIVERSITY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2);

			boolean bindUniversityCode = false;

			if (universityCode.isEmpty()) {
				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3);
			}
			else {
				bindUniversityCode = true;

				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindUniversityCode) {
					qPos.add(universityCode);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2 =
			"university.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2 =
			"university.universityCode = ?";

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3 =
			"(university.universityCode IS NULL OR university.universityCode = '')";

	private FinderPath _finderPathWithPaginationFindByGroupId_DataType;
	private FinderPath _finderPathWithoutPaginationFindByGroupId_DataType;
	private FinderPath _finderPathCountByGroupId_DataType;

	/**
	 * Returns all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the matching universities
	 */
	@Override
	public List<University> findByGroupId_DataType(
		long groupId, short dataType) {

		return findByGroupId_DataType(
			groupId, dataType, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of matching universities
	 */
	@Override
	public List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end) {

		return findByGroupId_DataType(groupId, dataType, start, end, null);
	}

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_DataType(long,short, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching universities
	 */
	@Deprecated
	@Override
	public List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator,
		boolean useFinderCache) {

		return findByGroupId_DataType(
			groupId, dataType, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the universities where groupId = &#63; and dataType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching universities
	 */
	@Override
	public List<University> findByGroupId_DataType(
		long groupId, short dataType, int start, int end,
		OrderByComparator<University> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByGroupId_DataType;
			finderArgs = new Object[] {groupId, dataType};
		}
		else {
			finderPath = _finderPathWithPaginationFindByGroupId_DataType;
			finderArgs = new Object[] {
				groupId, dataType, start, end, orderByComparator
			};
		}

		List<University> list = (List<University>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (University university : list) {
				if ((groupId != university.getGroupId()) ||
					(dataType != university.getDataType())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_UNIVERSITY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_DATATYPE_GROUPID_2);

			query.append(_FINDER_COLUMN_GROUPID_DATATYPE_DATATYPE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(UniversityModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(dataType);

				if (!pagination) {
					list = (List<University>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<University>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	@Override
	public University findByGroupId_DataType_First(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException {

		University university = fetchByGroupId_DataType_First(
			groupId, dataType, orderByComparator);

		if (university != null) {
			return university;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", dataType=");
		msg.append(dataType);

		msg.append("}");

		throw new NoSuchUniversityException(msg.toString());
	}

	/**
	 * Returns the first university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching university, or <code>null</code> if a matching university could not be found
	 */
	@Override
	public University fetchByGroupId_DataType_First(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator) {

		List<University> list = findByGroupId_DataType(
			groupId, dataType, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university
	 * @throws NoSuchUniversityException if a matching university could not be found
	 */
	@Override
	public University findByGroupId_DataType_Last(
			long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException {

		University university = fetchByGroupId_DataType_Last(
			groupId, dataType, orderByComparator);

		if (university != null) {
			return university;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", dataType=");
		msg.append(dataType);

		msg.append("}");

		throw new NoSuchUniversityException(msg.toString());
	}

	/**
	 * Returns the last university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching university, or <code>null</code> if a matching university could not be found
	 */
	@Override
	public University fetchByGroupId_DataType_Last(
		long groupId, short dataType,
		OrderByComparator<University> orderByComparator) {

		int count = countByGroupId_DataType(groupId, dataType);

		if (count == 0) {
			return null;
		}

		List<University> list = findByGroupId_DataType(
			groupId, dataType, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the universities before and after the current university in the ordered set where groupId = &#63; and dataType = &#63;.
	 *
	 * @param universityId the primary key of the current university
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	@Override
	public University[] findByGroupId_DataType_PrevAndNext(
			long universityId, long groupId, short dataType,
			OrderByComparator<University> orderByComparator)
		throws NoSuchUniversityException {

		University university = findByPrimaryKey(universityId);

		Session session = null;

		try {
			session = openSession();

			University[] array = new UniversityImpl[3];

			array[0] = getByGroupId_DataType_PrevAndNext(
				session, university, groupId, dataType, orderByComparator,
				true);

			array[1] = university;

			array[2] = getByGroupId_DataType_PrevAndNext(
				session, university, groupId, dataType, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected University getByGroupId_DataType_PrevAndNext(
		Session session, University university, long groupId, short dataType,
		OrderByComparator<University> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_UNIVERSITY_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_DATATYPE_GROUPID_2);

		query.append(_FINDER_COLUMN_GROUPID_DATATYPE_DATATYPE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UniversityModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		qPos.add(dataType);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(university)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<University> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the universities where groupId = &#63; and dataType = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 */
	@Override
	public void removeByGroupId_DataType(long groupId, short dataType) {
		for (University university :
				findByGroupId_DataType(
					groupId, dataType, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(university);
		}
	}

	/**
	 * Returns the number of universities where groupId = &#63; and dataType = &#63;.
	 *
	 * @param groupId the group ID
	 * @param dataType the data type
	 * @return the number of matching universities
	 */
	@Override
	public int countByGroupId_DataType(long groupId, short dataType) {
		FinderPath finderPath = _finderPathCountByGroupId_DataType;

		Object[] finderArgs = new Object[] {groupId, dataType};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_UNIVERSITY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_DATATYPE_GROUPID_2);

			query.append(_FINDER_COLUMN_GROUPID_DATATYPE_DATATYPE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(dataType);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_DATATYPE_GROUPID_2 =
		"university.groupId = ? AND ";

	private static final String _FINDER_COLUMN_GROUPID_DATATYPE_DATATYPE_2 =
		"university.dataType = ?";

	public UniversityPersistenceImpl() {
		setModelClass(University.class);

		setModelImplClass(UniversityImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the university in the entity cache if it is enabled.
	 *
	 * @param university the university
	 */
	@Override
	public void cacheResult(University university) {
		entityCache.putResult(
			entityCacheEnabled, UniversityImpl.class,
			university.getPrimaryKey(), university);

		finderCache.putResult(
			_finderPathFetchByGroupId_UniversityCode,
			new Object[] {
				university.getGroupId(), university.getUniversityCode()
			},
			university);

		university.resetOriginalValues();
	}

	/**
	 * Caches the universities in the entity cache if it is enabled.
	 *
	 * @param universities the universities
	 */
	@Override
	public void cacheResult(List<University> universities) {
		for (University university : universities) {
			if (entityCache.getResult(
					entityCacheEnabled, UniversityImpl.class,
					university.getPrimaryKey()) == null) {

				cacheResult(university);
			}
			else {
				university.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all universities.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UniversityImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the university.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(University university) {
		entityCache.removeResult(
			entityCacheEnabled, UniversityImpl.class,
			university.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((UniversityModelImpl)university, true);
	}

	@Override
	public void clearCache(List<University> universities) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (University university : universities) {
			entityCache.removeResult(
				entityCacheEnabled, UniversityImpl.class,
				university.getPrimaryKey());

			clearUniqueFindersCache((UniversityModelImpl)university, true);
		}
	}

	protected void cacheUniqueFindersCache(
		UniversityModelImpl universityModelImpl) {

		Object[] args = new Object[] {
			universityModelImpl.getGroupId(),
			universityModelImpl.getUniversityCode()
		};

		finderCache.putResult(
			_finderPathCountByGroupId_UniversityCode, args, Long.valueOf(1),
			false);
		finderCache.putResult(
			_finderPathFetchByGroupId_UniversityCode, args, universityModelImpl,
			false);
	}

	protected void clearUniqueFindersCache(
		UniversityModelImpl universityModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				universityModelImpl.getGroupId(),
				universityModelImpl.getUniversityCode()
			};

			finderCache.removeResult(
				_finderPathCountByGroupId_UniversityCode, args);
			finderCache.removeResult(
				_finderPathFetchByGroupId_UniversityCode, args);
		}

		if ((universityModelImpl.getColumnBitmask() &
			 _finderPathFetchByGroupId_UniversityCode.getColumnBitmask()) !=
				 0) {

			Object[] args = new Object[] {
				universityModelImpl.getOriginalGroupId(),
				universityModelImpl.getOriginalUniversityCode()
			};

			finderCache.removeResult(
				_finderPathCountByGroupId_UniversityCode, args);
			finderCache.removeResult(
				_finderPathFetchByGroupId_UniversityCode, args);
		}
	}

	/**
	 * Creates a new university with the primary key. Does not add the university to the database.
	 *
	 * @param universityId the primary key for the new university
	 * @return the new university
	 */
	@Override
	public University create(long universityId) {
		University university = new UniversityImpl();

		university.setNew(true);
		university.setPrimaryKey(universityId);

		university.setCompanyId(CompanyThreadLocal.getCompanyId());

		return university;
	}

	/**
	 * Removes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param universityId the primary key of the university
	 * @return the university that was removed
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	@Override
	public University remove(long universityId)
		throws NoSuchUniversityException {

		return remove((Serializable)universityId);
	}

	/**
	 * Removes the university with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the university
	 * @return the university that was removed
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	@Override
	public University remove(Serializable primaryKey)
		throws NoSuchUniversityException {

		Session session = null;

		try {
			session = openSession();

			University university = (University)session.get(
				UniversityImpl.class, primaryKey);

			if (university == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUniversityException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(university);
		}
		catch (NoSuchUniversityException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected University removeImpl(University university) {
		universityToStudentTableMapper.deleteLeftPrimaryKeyTableMappings(
			university.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(university)) {
				university = (University)session.get(
					UniversityImpl.class, university.getPrimaryKeyObj());
			}

			if (university != null) {
				session.delete(university);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (university != null) {
			clearCache(university);
		}

		return university;
	}

	@Override
	public University updateImpl(University university) {
		boolean isNew = university.isNew();

		if (!(university instanceof UniversityModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(university.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(university);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in university proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom University implementation " +
					university.getClass());
		}

		UniversityModelImpl universityModelImpl =
			(UniversityModelImpl)university;

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (university.getCreateDate() == null)) {
			if (serviceContext == null) {
				university.setCreateDate(now);
			}
			else {
				university.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!universityModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				university.setModifiedDate(now);
			}
			else {
				university.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (university.isNew()) {
				session.save(university);

				university.setNew(false);
			}
			else {
				university = (University)session.merge(university);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {
				universityModelImpl.getGroupId(),
				universityModelImpl.getDataType()
			};

			finderCache.removeResult(_finderPathCountByGroupId_DataType, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByGroupId_DataType, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((universityModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByGroupId_DataType.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					universityModelImpl.getOriginalGroupId(),
					universityModelImpl.getOriginalDataType()
				};

				finderCache.removeResult(
					_finderPathCountByGroupId_DataType, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByGroupId_DataType, args);

				args = new Object[] {
					universityModelImpl.getGroupId(),
					universityModelImpl.getDataType()
				};

				finderCache.removeResult(
					_finderPathCountByGroupId_DataType, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByGroupId_DataType, args);
			}
		}

		entityCache.putResult(
			entityCacheEnabled, UniversityImpl.class,
			university.getPrimaryKey(), university, false);

		clearUniqueFindersCache(universityModelImpl, false);
		cacheUniqueFindersCache(universityModelImpl);

		university.resetOriginalValues();

		return university;
	}

	/**
	 * Returns the university with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the university
	 * @return the university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	@Override
	public University findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUniversityException {

		University university = fetchByPrimaryKey(primaryKey);

		if (university == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUniversityException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return university;
	}

	/**
	 * Returns the university with the primary key or throws a <code>NoSuchUniversityException</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university
	 * @throws NoSuchUniversityException if a university with the primary key could not be found
	 */
	@Override
	public University findByPrimaryKey(long universityId)
		throws NoSuchUniversityException {

		return findByPrimaryKey((Serializable)universityId);
	}

	/**
	 * Returns the university with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param universityId the primary key of the university
	 * @return the university, or <code>null</code> if a university with the primary key could not be found
	 */
	@Override
	public University fetchByPrimaryKey(long universityId) {
		return fetchByPrimaryKey((Serializable)universityId);
	}

	/**
	 * Returns all the universities.
	 *
	 * @return the universities
	 */
	@Override
	public List<University> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of universities
	 */
	@Override
	public List<University> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of universities
	 */
	@Deprecated
	@Override
	public List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the universities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities
	 */
	@Override
	public List<University> findAll(
		int start, int end, OrderByComparator<University> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<University> list = (List<University>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_UNIVERSITY);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_UNIVERSITY;

				if (pagination) {
					sql = sql.concat(UniversityModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<University>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<University>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the universities from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (University university : findAll()) {
			remove(university);
		}
	}

	/**
	 * Returns the number of universities.
	 *
	 * @return the number of universities
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_UNIVERSITY);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return long[] of the primaryKeys of students associated with the university
	 */
	@Override
	public long[] getStudentPrimaryKeys(long pk) {
		long[] pks = universityToStudentTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the universities associated with the student
	 */
	@Override
	public List<University> getStudentUniversities(long pk) {
		return getStudentUniversities(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of universities associated with the student
	 */
	@Override
	public List<University> getStudentUniversities(
		long pk, int start, int end) {

		return getStudentUniversities(pk, start, end, null);
	}

	/**
	 * Returns all the university associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>UniversityModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of universities associated with the student
	 */
	@Override
	public List<University> getStudentUniversities(
		long pk, int start, int end,
		OrderByComparator<University> orderByComparator) {

		return universityToStudentTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of students associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the number of students associated with the university
	 */
	@Override
	public int getStudentsSize(long pk) {
		long[] pks = universityToStudentTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the student is associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 * @return <code>true</code> if the student is associated with the university; <code>false</code> otherwise
	 */
	@Override
	public boolean containsStudent(long pk, long studentPK) {
		return universityToStudentTableMapper.containsTableMapping(
			pk, studentPK);
	}

	/**
	 * Returns <code>true</code> if the university has any students associated with it.
	 *
	 * @param pk the primary key of the university to check for associations with students
	 * @return <code>true</code> if the university has any students associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsStudents(long pk) {
		if (getStudentsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	@Override
	public void addStudent(long pk, long studentPK) {
		University university = fetchByPrimaryKey(pk);

		if (university == null) {
			universityToStudentTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, studentPK);
		}
		else {
			universityToStudentTableMapper.addTableMapping(
				university.getCompanyId(), pk, studentPK);
		}
	}

	/**
	 * Adds an association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	@Override
	public void addStudent(long pk, Student student) {
		University university = fetchByPrimaryKey(pk);

		if (university == null) {
			universityToStudentTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, student.getPrimaryKey());
		}
		else {
			universityToStudentTableMapper.addTableMapping(
				university.getCompanyId(), pk, student.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	@Override
	public void addStudents(long pk, long[] studentPKs) {
		long companyId = 0;

		University university = fetchByPrimaryKey(pk);

		if (university == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = university.getCompanyId();
		}

		universityToStudentTableMapper.addTableMappings(
			companyId, pk, studentPKs);
	}

	/**
	 * Adds an association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	@Override
	public void addStudents(long pk, List<Student> students) {
		addStudents(
			pk, ListUtil.toLongArray(students, Student.STUDENT_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the university and its students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university to clear the associated students from
	 */
	@Override
	public void clearStudents(long pk) {
		universityToStudentTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPK the primary key of the student
	 */
	@Override
	public void removeStudent(long pk, long studentPK) {
		universityToStudentTableMapper.deleteTableMapping(pk, studentPK);
	}

	/**
	 * Removes the association between the university and the student. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param student the student
	 */
	@Override
	public void removeStudent(long pk, Student student) {
		universityToStudentTableMapper.deleteTableMapping(
			pk, student.getPrimaryKey());
	}

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students
	 */
	@Override
	public void removeStudents(long pk, long[] studentPKs) {
		universityToStudentTableMapper.deleteTableMappings(pk, studentPKs);
	}

	/**
	 * Removes the association between the university and the students. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students
	 */
	@Override
	public void removeStudents(long pk, List<Student> students) {
		removeStudents(
			pk, ListUtil.toLongArray(students, Student.STUDENT_ID_ACCESSOR));
	}

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param studentPKs the primary keys of the students to be associated with the university
	 */
	@Override
	public void setStudents(long pk, long[] studentPKs) {
		Set<Long> newStudentPKsSet = SetUtil.fromArray(studentPKs);
		Set<Long> oldStudentPKsSet = SetUtil.fromArray(
			universityToStudentTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeStudentPKsSet = new HashSet<Long>(oldStudentPKsSet);

		removeStudentPKsSet.removeAll(newStudentPKsSet);

		universityToStudentTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeStudentPKsSet));

		newStudentPKsSet.removeAll(oldStudentPKsSet);

		long companyId = 0;

		University university = fetchByPrimaryKey(pk);

		if (university == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = university.getCompanyId();
		}

		universityToStudentTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newStudentPKsSet));
	}

	/**
	 * Sets the students associated with the university, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the university
	 * @param students the students to be associated with the university
	 */
	@Override
	public void setStudents(long pk, List<Student> students) {
		try {
			long[] studentPKs = new long[students.size()];

			for (int i = 0; i < students.size(); i++) {
				Student student = students.get(i);

				studentPKs[i] = student.getPrimaryKey();
			}

			setStudents(pk, studentPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "universityId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_UNIVERSITY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UniversityModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the university persistence.
	 */
	@Activate
	public void activate() {
		UniversityModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		UniversityModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		universityToStudentTableMapper = TableMapperFactory.getTableMapper(
			"hiree_Student_University#universityId", "hiree_Student_University",
			"companyId", "universityId", "studentId", this, Student.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, UniversityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, UniversityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathFetchByGroupId_UniversityCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, UniversityImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByGroupId_UniversityCode",
			new String[] {Long.class.getName(), String.class.getName()},
			UniversityModelImpl.GROUPID_COLUMN_BITMASK |
			UniversityModelImpl.UNIVERSITYCODE_COLUMN_BITMASK);

		_finderPathCountByGroupId_UniversityCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupId_UniversityCode",
			new String[] {Long.class.getName(), String.class.getName()});

		_finderPathWithPaginationFindByGroupId_DataType = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, UniversityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId_DataType",
			new String[] {
				Long.class.getName(), Short.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByGroupId_DataType = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, UniversityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId_DataType",
			new String[] {Long.class.getName(), Short.class.getName()},
			UniversityModelImpl.GROUPID_COLUMN_BITMASK |
			UniversityModelImpl.DATATYPE_COLUMN_BITMASK);

		_finderPathCountByGroupId_DataType = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupId_DataType",
			new String[] {Long.class.getName(), Short.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(UniversityImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper(
			"hiree_Student_University#universityId");
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.it.hiree.register.model.University"),
			true);
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<University, Student> universityToStudentTableMapper;

	private static final String _SQL_SELECT_UNIVERSITY =
		"SELECT university FROM University university";

	private static final String _SQL_SELECT_UNIVERSITY_WHERE =
		"SELECT university FROM University university WHERE ";

	private static final String _SQL_COUNT_UNIVERSITY =
		"SELECT COUNT(university) FROM University university";

	private static final String _SQL_COUNT_UNIVERSITY_WHERE =
		"SELECT COUNT(university) FROM University university WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "university.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No University exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No University exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		UniversityPersistenceImpl.class);

	static {
		try {
			Class.forName(hireePersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ExceptionInInitializerError(cnfe);
		}
	}

}