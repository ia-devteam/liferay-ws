/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;

import it.hiree.register.exception.NoSuchStudentException;
import it.hiree.register.model.Student;
import it.hiree.register.model.University;
import it.hiree.register.model.impl.StudentImpl;
import it.hiree.register.model.impl.StudentModelImpl;
import it.hiree.register.service.persistence.StudentPersistence;
import it.hiree.register.service.persistence.impl.constants.hireePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = StudentPersistence.class)
@ProviderType
public class StudentPersistenceImpl
	extends BasePersistenceImpl<Student> implements StudentPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>StudentUtil</code> to access the student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		StudentImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByGroupId_UniversityCode;
	private FinderPath _finderPathWithoutPaginationFindByGroupId_UniversityCode;
	private FinderPath _finderPathCountByGroupId_UniversityCode;

	/**
	 * Returns all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the matching students
	 */
	@Override
	public List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode) {

		return findByGroupId_UniversityCode(
			groupId, universityCode, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of matching students
	 */
	@Override
	public List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end) {

		return findByGroupId_UniversityCode(
			groupId, universityCode, start, end, null);
	}

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findByGroupId_UniversityCode(long,String, int, int, OrderByComparator)}
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching students
	 */
	@Deprecated
	@Override
	public List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator, boolean useFinderCache) {

		return findByGroupId_UniversityCode(
			groupId, universityCode, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the students where groupId = &#63; and universityCode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching students
	 */
	@Override
	public List<Student> findByGroupId_UniversityCode(
		long groupId, String universityCode, int start, int end,
		OrderByComparator<Student> orderByComparator) {

		universityCode = Objects.toString(universityCode, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath =
				_finderPathWithoutPaginationFindByGroupId_UniversityCode;
			finderArgs = new Object[] {groupId, universityCode};
		}
		else {
			finderPath = _finderPathWithPaginationFindByGroupId_UniversityCode;
			finderArgs = new Object[] {
				groupId, universityCode, start, end, orderByComparator
			};
		}

		List<Student> list = (List<Student>)finderCache.getResult(
			finderPath, finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Student student : list) {
				if ((groupId != student.getGroupId()) ||
					!universityCode.equals(student.getUniversityCode())) {

					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_STUDENT_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2);

			boolean bindUniversityCode = false;

			if (universityCode.isEmpty()) {
				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3);
			}
			else {
				bindUniversityCode = true;

				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(StudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindUniversityCode) {
					qPos.add(universityCode);
				}

				if (!pagination) {
					list = (List<Student>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Student>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	@Override
	public Student findByGroupId_UniversityCode_First(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException {

		Student student = fetchByGroupId_UniversityCode_First(
			groupId, universityCode, orderByComparator);

		if (student != null) {
			return student;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", universityCode=");
		msg.append(universityCode);

		msg.append("}");

		throw new NoSuchStudentException(msg.toString());
	}

	/**
	 * Returns the first student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student, or <code>null</code> if a matching student could not be found
	 */
	@Override
	public Student fetchByGroupId_UniversityCode_First(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator) {

		List<Student> list = findByGroupId_UniversityCode(
			groupId, universityCode, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student
	 * @throws NoSuchStudentException if a matching student could not be found
	 */
	@Override
	public Student findByGroupId_UniversityCode_Last(
			long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException {

		Student student = fetchByGroupId_UniversityCode_Last(
			groupId, universityCode, orderByComparator);

		if (student != null) {
			return student;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", universityCode=");
		msg.append(universityCode);

		msg.append("}");

		throw new NoSuchStudentException(msg.toString());
	}

	/**
	 * Returns the last student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student, or <code>null</code> if a matching student could not be found
	 */
	@Override
	public Student fetchByGroupId_UniversityCode_Last(
		long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator) {

		int count = countByGroupId_UniversityCode(groupId, universityCode);

		if (count == 0) {
			return null;
		}

		List<Student> list = findByGroupId_UniversityCode(
			groupId, universityCode, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the students before and after the current student in the ordered set where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param studentId the primary key of the current student
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	@Override
	public Student[] findByGroupId_UniversityCode_PrevAndNext(
			long studentId, long groupId, String universityCode,
			OrderByComparator<Student> orderByComparator)
		throws NoSuchStudentException {

		universityCode = Objects.toString(universityCode, "");

		Student student = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			Student[] array = new StudentImpl[3];

			array[0] = getByGroupId_UniversityCode_PrevAndNext(
				session, student, groupId, universityCode, orderByComparator,
				true);

			array[1] = student;

			array[2] = getByGroupId_UniversityCode_PrevAndNext(
				session, student, groupId, universityCode, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Student getByGroupId_UniversityCode_PrevAndNext(
		Session session, Student student, long groupId, String universityCode,
		OrderByComparator<Student> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_STUDENT_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2);

		boolean bindUniversityCode = false;

		if (universityCode.isEmpty()) {
			query.append(
				_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3);
		}
		else {
			bindUniversityCode = true;

			query.append(
				_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(StudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (bindUniversityCode) {
			qPos.add(universityCode);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(student)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Student> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the students where groupId = &#63; and universityCode = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 */
	@Override
	public void removeByGroupId_UniversityCode(
		long groupId, String universityCode) {

		for (Student student :
				findByGroupId_UniversityCode(
					groupId, universityCode, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(student);
		}
	}

	/**
	 * Returns the number of students where groupId = &#63; and universityCode = &#63;.
	 *
	 * @param groupId the group ID
	 * @param universityCode the university code
	 * @return the number of matching students
	 */
	@Override
	public int countByGroupId_UniversityCode(
		long groupId, String universityCode) {

		universityCode = Objects.toString(universityCode, "");

		FinderPath finderPath = _finderPathCountByGroupId_UniversityCode;

		Object[] finderArgs = new Object[] {groupId, universityCode};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENT_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2);

			boolean bindUniversityCode = false;

			if (universityCode.isEmpty()) {
				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3);
			}
			else {
				bindUniversityCode = true;

				query.append(
					_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (bindUniversityCode) {
					qPos.add(universityCode);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_GROUPID_2 =
			"student.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_2 =
			"student.universityCode = ?";

	private static final String
		_FINDER_COLUMN_GROUPID_UNIVERSITYCODE_UNIVERSITYCODE_3 =
			"(student.universityCode IS NULL OR student.universityCode = '')";

	public StudentPersistenceImpl() {
		setModelClass(Student.class);

		setModelImplClass(StudentImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the student in the entity cache if it is enabled.
	 *
	 * @param student the student
	 */
	@Override
	public void cacheResult(Student student) {
		entityCache.putResult(
			entityCacheEnabled, StudentImpl.class, student.getPrimaryKey(),
			student);

		student.resetOriginalValues();
	}

	/**
	 * Caches the students in the entity cache if it is enabled.
	 *
	 * @param students the students
	 */
	@Override
	public void cacheResult(List<Student> students) {
		for (Student student : students) {
			if (entityCache.getResult(
					entityCacheEnabled, StudentImpl.class,
					student.getPrimaryKey()) == null) {

				cacheResult(student);
			}
			else {
				student.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all students.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(StudentImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Student student) {
		entityCache.removeResult(
			entityCacheEnabled, StudentImpl.class, student.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Student> students) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Student student : students) {
			entityCache.removeResult(
				entityCacheEnabled, StudentImpl.class, student.getPrimaryKey());
		}
	}

	/**
	 * Creates a new student with the primary key. Does not add the student to the database.
	 *
	 * @param studentId the primary key for the new student
	 * @return the new student
	 */
	@Override
	public Student create(long studentId) {
		Student student = new StudentImpl();

		student.setNew(true);
		student.setPrimaryKey(studentId);

		student.setCompanyId(CompanyThreadLocal.getCompanyId());

		return student;
	}

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the student
	 * @return the student that was removed
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	@Override
	public Student remove(long studentId) throws NoSuchStudentException {
		return remove((Serializable)studentId);
	}

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student
	 * @return the student that was removed
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	@Override
	public Student remove(Serializable primaryKey)
		throws NoSuchStudentException {

		Session session = null;

		try {
			session = openSession();

			Student student = (Student)session.get(
				StudentImpl.class, primaryKey);

			if (student == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(student);
		}
		catch (NoSuchStudentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Student removeImpl(Student student) {
		studentToUniversityTableMapper.deleteLeftPrimaryKeyTableMappings(
			student.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(student)) {
				student = (Student)session.get(
					StudentImpl.class, student.getPrimaryKeyObj());
			}

			if (student != null) {
				session.delete(student);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (student != null) {
			clearCache(student);
		}

		return student;
	}

	@Override
	public Student updateImpl(Student student) {
		boolean isNew = student.isNew();

		if (!(student instanceof StudentModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(student.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(student);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in student proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Student implementation " +
					student.getClass());
		}

		StudentModelImpl studentModelImpl = (StudentModelImpl)student;

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (student.getCreateDate() == null)) {
			if (serviceContext == null) {
				student.setCreateDate(now);
			}
			else {
				student.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!studentModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				student.setModifiedDate(now);
			}
			else {
				student.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (student.isNew()) {
				session.save(student);

				student.setNew(false);
			}
			else {
				student = (Student)session.merge(student);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {
				studentModelImpl.getGroupId(),
				studentModelImpl.getUniversityCode()
			};

			finderCache.removeResult(
				_finderPathCountByGroupId_UniversityCode, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByGroupId_UniversityCode, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((studentModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByGroupId_UniversityCode.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					studentModelImpl.getOriginalGroupId(),
					studentModelImpl.getOriginalUniversityCode()
				};

				finderCache.removeResult(
					_finderPathCountByGroupId_UniversityCode, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByGroupId_UniversityCode,
					args);

				args = new Object[] {
					studentModelImpl.getGroupId(),
					studentModelImpl.getUniversityCode()
				};

				finderCache.removeResult(
					_finderPathCountByGroupId_UniversityCode, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByGroupId_UniversityCode,
					args);
			}
		}

		entityCache.putResult(
			entityCacheEnabled, StudentImpl.class, student.getPrimaryKey(),
			student, false);

		student.resetOriginalValues();

		return student;
	}

	/**
	 * Returns the student with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student
	 * @return the student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	@Override
	public Student findByPrimaryKey(Serializable primaryKey)
		throws NoSuchStudentException {

		Student student = fetchByPrimaryKey(primaryKey);

		if (student == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchStudentException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return student;
	}

	/**
	 * Returns the student with the primary key or throws a <code>NoSuchStudentException</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student
	 * @throws NoSuchStudentException if a student with the primary key could not be found
	 */
	@Override
	public Student findByPrimaryKey(long studentId)
		throws NoSuchStudentException {

		return findByPrimaryKey((Serializable)studentId);
	}

	/**
	 * Returns the student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student, or <code>null</code> if a student with the primary key could not be found
	 */
	@Override
	public Student fetchByPrimaryKey(long studentId) {
		return fetchByPrimaryKey((Serializable)studentId);
	}

	/**
	 * Returns all the students.
	 *
	 * @return the students
	 */
	@Override
	public List<Student> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of students
	 */
	@Override
	public List<Student> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of students
	 */
	@Deprecated
	@Override
	public List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students
	 */
	@Override
	public List<Student> findAll(
		int start, int end, OrderByComparator<Student> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Student> list = (List<Student>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_STUDENT);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENT;

				if (pagination) {
					sql = sql.concat(StudentModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Student>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Student>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the students from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Student student : findAll()) {
			remove(student);
		}
	}

	/**
	 * Returns the number of students.
	 *
	 * @return the number of students
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return long[] of the primaryKeys of universities associated with the student
	 */
	@Override
	public long[] getUniversityPrimaryKeys(long pk) {
		long[] pks = studentToUniversityTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * @param pk the primary key of the university
	 * @return the students associated with the university
	 */
	@Override
	public List<Student> getUniversityStudents(long pk) {
		return getUniversityStudents(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @return the range of students associated with the university
	 */
	@Override
	public List<Student> getUniversityStudents(long pk, int start, int end) {
		return getUniversityStudents(pk, start, end, null);
	}

	/**
	 * Returns all the student associated with the university.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>StudentModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the university
	 * @param start the lower bound of the range of universities
	 * @param end the upper bound of the range of universities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students associated with the university
	 */
	@Override
	public List<Student> getUniversityStudents(
		long pk, int start, int end,
		OrderByComparator<Student> orderByComparator) {

		return studentToUniversityTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of universities associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of universities associated with the student
	 */
	@Override
	public int getUniversitiesSize(long pk) {
		long[] pks = studentToUniversityTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the university is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 * @return <code>true</code> if the university is associated with the student; <code>false</code> otherwise
	 */
	@Override
	public boolean containsUniversity(long pk, long universityPK) {
		return studentToUniversityTableMapper.containsTableMapping(
			pk, universityPK);
	}

	/**
	 * Returns <code>true</code> if the student has any universities associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with universities
	 * @return <code>true</code> if the student has any universities associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsUniversities(long pk) {
		if (getUniversitiesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	@Override
	public void addUniversity(long pk, long universityPK) {
		Student student = fetchByPrimaryKey(pk);

		if (student == null) {
			studentToUniversityTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, universityPK);
		}
		else {
			studentToUniversityTableMapper.addTableMapping(
				student.getCompanyId(), pk, universityPK);
		}
	}

	/**
	 * Adds an association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	@Override
	public void addUniversity(long pk, University university) {
		Student student = fetchByPrimaryKey(pk);

		if (student == null) {
			studentToUniversityTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk,
				university.getPrimaryKey());
		}
		else {
			studentToUniversityTableMapper.addTableMapping(
				student.getCompanyId(), pk, university.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	@Override
	public void addUniversities(long pk, long[] universityPKs) {
		long companyId = 0;

		Student student = fetchByPrimaryKey(pk);

		if (student == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = student.getCompanyId();
		}

		studentToUniversityTableMapper.addTableMappings(
			companyId, pk, universityPKs);
	}

	/**
	 * Adds an association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	@Override
	public void addUniversities(long pk, List<University> universities) {
		addUniversities(
			pk,
			ListUtil.toLongArray(
				universities, University.UNIVERSITY_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the student and its universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student to clear the associated universities from
	 */
	@Override
	public void clearUniversities(long pk) {
		studentToUniversityTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPK the primary key of the university
	 */
	@Override
	public void removeUniversity(long pk, long universityPK) {
		studentToUniversityTableMapper.deleteTableMapping(pk, universityPK);
	}

	/**
	 * Removes the association between the student and the university. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param university the university
	 */
	@Override
	public void removeUniversity(long pk, University university) {
		studentToUniversityTableMapper.deleteTableMapping(
			pk, university.getPrimaryKey());
	}

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities
	 */
	@Override
	public void removeUniversities(long pk, long[] universityPKs) {
		studentToUniversityTableMapper.deleteTableMappings(pk, universityPKs);
	}

	/**
	 * Removes the association between the student and the universities. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities
	 */
	@Override
	public void removeUniversities(long pk, List<University> universities) {
		removeUniversities(
			pk,
			ListUtil.toLongArray(
				universities, University.UNIVERSITY_ID_ACCESSOR));
	}

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universityPKs the primary keys of the universities to be associated with the student
	 */
	@Override
	public void setUniversities(long pk, long[] universityPKs) {
		Set<Long> newUniversityPKsSet = SetUtil.fromArray(universityPKs);
		Set<Long> oldUniversityPKsSet = SetUtil.fromArray(
			studentToUniversityTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeUniversityPKsSet = new HashSet<Long>(
			oldUniversityPKsSet);

		removeUniversityPKsSet.removeAll(newUniversityPKsSet);

		studentToUniversityTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeUniversityPKsSet));

		newUniversityPKsSet.removeAll(oldUniversityPKsSet);

		long companyId = 0;

		Student student = fetchByPrimaryKey(pk);

		if (student == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = student.getCompanyId();
		}

		studentToUniversityTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newUniversityPKsSet));
	}

	/**
	 * Sets the universities associated with the student, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the student
	 * @param universities the universities to be associated with the student
	 */
	@Override
	public void setUniversities(long pk, List<University> universities) {
		try {
			long[] universityPKs = new long[universities.size()];

			for (int i = 0; i < universities.size(); i++) {
				University university = universities.get(i);

				universityPKs[i] = university.getPrimaryKey();
			}

			setUniversities(pk, universityPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "studentId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_STUDENT;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return StudentModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the student persistence.
	 */
	@Activate
	public void activate() {
		StudentModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		StudentModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		studentToUniversityTableMapper = TableMapperFactory.getTableMapper(
			"hiree_Student_University#studentId", "hiree_Student_University",
			"companyId", "studentId", "universityId", this, University.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, StudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, StudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByGroupId_UniversityCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, StudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByGroupId_UniversityCode",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByGroupId_UniversityCode =
			new FinderPath(
				entityCacheEnabled, finderCacheEnabled, StudentImpl.class,
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByGroupId_UniversityCode",
				new String[] {Long.class.getName(), String.class.getName()},
				StudentModelImpl.GROUPID_COLUMN_BITMASK |
				StudentModelImpl.UNIVERSITYCODE_COLUMN_BITMASK |
				StudentModelImpl.NAME_COLUMN_BITMASK |
				StudentModelImpl.CREATEDATE_COLUMN_BITMASK);

		_finderPathCountByGroupId_UniversityCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupId_UniversityCode",
			new String[] {Long.class.getName(), String.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(StudentImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper(
			"hiree_Student_University#studentId");
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.it.hiree.register.model.Student"),
			true);
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = hireePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<Student, University> studentToUniversityTableMapper;

	private static final String _SQL_SELECT_STUDENT =
		"SELECT student FROM Student student";

	private static final String _SQL_SELECT_STUDENT_WHERE =
		"SELECT student FROM Student student WHERE ";

	private static final String _SQL_COUNT_STUDENT =
		"SELECT COUNT(student) FROM Student student";

	private static final String _SQL_COUNT_STUDENT_WHERE =
		"SELECT COUNT(student) FROM Student student WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "student.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Student exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Student exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		StudentPersistenceImpl.class);

	static {
		try {
			Class.forName(hireePersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ExceptionInInitializerError(cnfe);
		}
	}

}