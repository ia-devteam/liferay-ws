/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import it.hiree.register.exception.NoSuchUniversityException;
import it.hiree.register.model.Student;
import it.hiree.register.model.University;
import it.hiree.register.service.UniversityLocalService;
import it.hiree.register.service.base.StudentLocalServiceBaseImpl;

import it.hiree.register.constant.UniversityConstant;

/**
 * The implementation of the student local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>it.hiree.register.service.StudentLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StudentLocalServiceBaseImpl
 */
@Component(property = "model.class.name=it.hiree.register.model.Student", service = AopService.class)
public class StudentLocalServiceImpl extends StudentLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>it.hiree.register.service.StudentLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>it.hiree.register.service.StudentLocalServiceUtil</code>.
	 */

	public Student createStudent(long userId, long groupId, long companyId, String name, String surName,
			String emailAddress, String password, String universityName, String universityCode, String course, boolean finished,
			ServiceContext serviceContext) throws SystemException, PortalException {
		boolean autoPassword = false;
		boolean autoScreenName = true;
		boolean sendEmail = true;

		long[] groupIds = new long[] { groupId };
		long[] organizationIds = null;
		long[] roleIds = null;
		long[] userGroupIds = null;

		String screenName = null;

		Role defaultGroupRole = _roleLocalService.getDefaultGroupRole(groupId);

		if (defaultGroupRole != null) {
			roleIds = new long[] { defaultGroupRole.getRoleId() };
		}

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 20);

		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

		User user = userLocalService.addUserWithWorkflow(0, companyId, autoPassword, password, password, autoScreenName,
				screenName, emailAddress, 0L, StringPool.BLANK, serviceContext.getLocale(), name, StringPool.BLANK,
				surName, 0, 0, true, month, dayOfMonth, year, finished ? "student-now" : "“student-in-the-past", groupIds, organizationIds, roleIds,
				userGroupIds, sendEmail, serviceContext);

		userLocalService.updateAgreedToTermsOfUse(user.getUserId(), true);
		userLocalService.updatePasswordReset(user.getUserId(), false);

		long studentId = counterLocalService.increment(Student.class.getName(), 1);

		Student student = studentPersistence.create(studentId);

		Date now = new Date();
		student.setCompanyId(companyId);
		student.setCreateDate(now);
		student.setGroupId(groupId);
		student.setModifiedDate(now);
		student.setName(name);
		student.setSurName(surName);
		student.setUniversityCode(universityCode);
		student.setMappingUserId(user.getUserId());
		student.setCourse(course);
		student.setFinished(finished);
		University university = _universityLocalService.createUniversity(userId, groupId, companyId, surName,
				universityCode, StringPool.BLANK, UniversityConstant.CREATE_NEW, serviceContext);

		if (university == null) {
			throw new NoSuchUniversityException();
		}
		studentPersistence.addUniversity(studentId, university.getUniversityId());
		
		return studentPersistence.update(student);
	}

	@Reference(unbind = "-")
	protected void setUserLocalService(UniversityLocalService universityLocalService) {
		_universityLocalService = universityLocalService;
	}

	protected UniversityLocalService _universityLocalService;

	@Reference(unbind = "-")
	protected void setRoleLocalService(RoleLocalService roleLocalService) {
		_roleLocalService = roleLocalService;
	}

	private RoleLocalService _roleLocalService;
}