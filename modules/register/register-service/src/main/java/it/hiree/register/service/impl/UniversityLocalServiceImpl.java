/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import it.hiree.register.exception.NoSuchUniversityException;
import it.hiree.register.model.University;
import it.hiree.register.service.base.UniversityLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the university local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>it.hiree.register.service.UniversityLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UniversityLocalServiceBaseImpl
 */
@Component(property = "model.class.name=it.hiree.register.model.University", service = AopService.class)
public class UniversityLocalServiceImpl extends UniversityLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>it.hiree.register.service.UniversityLocalService</code> via injection or
	 * a <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>it.hiree.register.service.UniversityLocalServiceUtil</code>.
	 */

	public University createUniversity(long userId, long groupId, long companyId, String name, String universityCode,
			String url, short dataType, ServiceContext serviceContext) {
		long universityId = counterLocalService.increment(University.class.getName(), 1);
		University university = universityPersistence.create(universityId);
		User user = userLocalService.fetchUser(userId);
		Date now = new Date();
		university.setCompanyId(companyId);
		university.setCreateDate(now);
		university.setGroupId(groupId);
		university.setModifiedDate(now);
		university.setUserName(user != null ? user.getFullName() : StringPool.BLANK);
		university.setName(name);
		university.setUniversityCode(universityCode);
		university.setUrl(url);
		university.setDataType(dataType);
		return universityPersistence.update(university);
	}

	public University updateUniversity(long userId, long groupId, long companyId, long universityId, String name,
			String universityCode, String url, short dataType, ServiceContext serviceContext)
			throws NoSuchUniversityException {
		University university = universityPersistence.findByPrimaryKey(universityId);
		User user = userLocalService.fetchUser(userId);
		Date now = new Date();
		university.setGroupId(groupId);
		university.setModifiedDate(now);
		university.setUserName(user != null ? user.getFullName() : StringPool.BLANK);
		university.setName(name);
		university.setUniversityCode(universityCode);
		university.setUrl(url);
		university.setDataType(dataType);
		return universityPersistence.update(university);
	}

	public University fetchByGroupId_UniversityCode(long groupId, String universityCode) {
		return universityPersistence.fetchByGroupId_UniversityCode(groupId, universityCode);
	}

	public University findByGroupId_UniversityCode(long groupId, String universityCode)
			throws NoSuchUniversityException {
		return universityPersistence.findByGroupId_UniversityCode(groupId, universityCode);
	}

	public List<University> findAll(int start, int end) {
		return universityPersistence.findAll(start, end);
	}

	public List<University> findByGroupId_DataType(long groupId, short dataType) {
		return universityPersistence.findByGroupId_DataType(groupId, dataType);
	}

}