/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.hiree.register.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import it.hiree.register.exception.NoSuchUniversityException;
import it.hiree.register.model.University;
import it.hiree.register.service.UniversityLocalServiceUtil;
import it.hiree.register.service.persistence.UniversityPersistence;
import it.hiree.register.service.persistence.UniversityUtil;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class UniversityPersistenceTest {

	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule =
		new AggregateTestRule(
			new LiferayIntegrationTestRule(), PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(
				Propagation.REQUIRED, "it.hiree.register.service"));

	@Before
	public void setUp() {
		_persistence = UniversityUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<University> iterator = _universities.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		University university = _persistence.create(pk);

		Assert.assertNotNull(university);

		Assert.assertEquals(university.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		University newUniversity = addUniversity();

		_persistence.remove(newUniversity);

		University existingUniversity = _persistence.fetchByPrimaryKey(
			newUniversity.getPrimaryKey());

		Assert.assertNull(existingUniversity);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addUniversity();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		University newUniversity = _persistence.create(pk);

		newUniversity.setGroupId(RandomTestUtil.nextLong());

		newUniversity.setCompanyId(RandomTestUtil.nextLong());

		newUniversity.setUserId(RandomTestUtil.nextLong());

		newUniversity.setUserName(RandomTestUtil.randomString());

		newUniversity.setCreateDate(RandomTestUtil.nextDate());

		newUniversity.setModifiedDate(RandomTestUtil.nextDate());

		newUniversity.setName(RandomTestUtil.randomString());

		newUniversity.setUniversityCode(RandomTestUtil.randomString());

		newUniversity.setUrl(RandomTestUtil.randomString());

		newUniversity.setDataType();

		_universities.add(_persistence.update(newUniversity));

		University existingUniversity = _persistence.findByPrimaryKey(
			newUniversity.getPrimaryKey());

		Assert.assertEquals(
			existingUniversity.getUniversityId(),
			newUniversity.getUniversityId());
		Assert.assertEquals(
			existingUniversity.getGroupId(), newUniversity.getGroupId());
		Assert.assertEquals(
			existingUniversity.getCompanyId(), newUniversity.getCompanyId());
		Assert.assertEquals(
			existingUniversity.getUserId(), newUniversity.getUserId());
		Assert.assertEquals(
			existingUniversity.getUserName(), newUniversity.getUserName());
		Assert.assertEquals(
			Time.getShortTimestamp(existingUniversity.getCreateDate()),
			Time.getShortTimestamp(newUniversity.getCreateDate()));
		Assert.assertEquals(
			Time.getShortTimestamp(existingUniversity.getModifiedDate()),
			Time.getShortTimestamp(newUniversity.getModifiedDate()));
		Assert.assertEquals(
			existingUniversity.getName(), newUniversity.getName());
		Assert.assertEquals(
			existingUniversity.getUniversityCode(),
			newUniversity.getUniversityCode());
		Assert.assertEquals(
			existingUniversity.getUrl(), newUniversity.getUrl());
		Assert.assertEquals(
			existingUniversity.getDataType(), newUniversity.getDataType());
	}

	@Test
	public void testCountByGroupId_UniversityCode() throws Exception {
		_persistence.countByGroupId_UniversityCode(
			RandomTestUtil.nextLong(), "");

		_persistence.countByGroupId_UniversityCode(0L, "null");

		_persistence.countByGroupId_UniversityCode(0L, (String)null);
	}

	@Test
	public void testCountByGroupId_DataType() throws Exception {
		_persistence.countByGroupId_DataType(
			RandomTestUtil.nextLong(), (short)null);

		_persistence.countByGroupId_DataType(0L, (short)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		University newUniversity = addUniversity();

		University existingUniversity = _persistence.findByPrimaryKey(
			newUniversity.getPrimaryKey());

		Assert.assertEquals(existingUniversity, newUniversity);
	}

	@Test(expected = NoSuchUniversityException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, getOrderByComparator());
	}

	protected OrderByComparator<University> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create(
			"hiree_University", "universityId", true, "groupId", true,
			"companyId", true, "userId", true, "userName", true, "createDate",
			true, "modifiedDate", true, "name", true, "universityCode", true,
			"url", true, "dataType", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		University newUniversity = addUniversity();

		University existingUniversity = _persistence.fetchByPrimaryKey(
			newUniversity.getPrimaryKey());

		Assert.assertEquals(existingUniversity, newUniversity);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		University missingUniversity = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingUniversity);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {

		University newUniversity1 = addUniversity();
		University newUniversity2 = addUniversity();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUniversity1.getPrimaryKey());
		primaryKeys.add(newUniversity2.getPrimaryKey());

		Map<Serializable, University> universities =
			_persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, universities.size());
		Assert.assertEquals(
			newUniversity1, universities.get(newUniversity1.getPrimaryKey()));
		Assert.assertEquals(
			newUniversity2, universities.get(newUniversity2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {

		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, University> universities =
			_persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(universities.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {

		University newUniversity = addUniversity();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUniversity.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, University> universities =
			_persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, universities.size());
		Assert.assertEquals(
			newUniversity, universities.get(newUniversity.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys() throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, University> universities =
			_persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(universities.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey() throws Exception {
		University newUniversity = addUniversity();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUniversity.getPrimaryKey());

		Map<Serializable, University> universities =
			_persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, universities.size());
		Assert.assertEquals(
			newUniversity, universities.get(newUniversity.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery =
			UniversityLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(
			new ActionableDynamicQuery.PerformActionMethod<University>() {

				@Override
				public void performAction(University university) {
					Assert.assertNotNull(university);

					count.increment();
				}

			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting() throws Exception {
		University newUniversity = addUniversity();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
			University.class, _dynamicQueryClassLoader);

		dynamicQuery.add(
			RestrictionsFactoryUtil.eq(
				"universityId", newUniversity.getUniversityId()));

		List<University> result = _persistence.findWithDynamicQuery(
			dynamicQuery);

		Assert.assertEquals(1, result.size());

		University existingUniversity = result.get(0);

		Assert.assertEquals(existingUniversity, newUniversity);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
			University.class, _dynamicQueryClassLoader);

		dynamicQuery.add(
			RestrictionsFactoryUtil.eq(
				"universityId", RandomTestUtil.nextLong()));

		List<University> result = _persistence.findWithDynamicQuery(
			dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting() throws Exception {
		University newUniversity = addUniversity();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
			University.class, _dynamicQueryClassLoader);

		dynamicQuery.setProjection(
			ProjectionFactoryUtil.property("universityId"));

		Object newUniversityId = newUniversity.getUniversityId();

		dynamicQuery.add(
			RestrictionsFactoryUtil.in(
				"universityId", new Object[] {newUniversityId}));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingUniversityId = result.get(0);

		Assert.assertEquals(existingUniversityId, newUniversityId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
			University.class, _dynamicQueryClassLoader);

		dynamicQuery.setProjection(
			ProjectionFactoryUtil.property("universityId"));

		dynamicQuery.add(
			RestrictionsFactoryUtil.in(
				"universityId", new Object[] {RandomTestUtil.nextLong()}));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		University newUniversity = addUniversity();

		_persistence.clearCache();

		University existingUniversity = _persistence.findByPrimaryKey(
			newUniversity.getPrimaryKey());

		Assert.assertEquals(
			Long.valueOf(existingUniversity.getGroupId()),
			ReflectionTestUtil.<Long>invoke(
				existingUniversity, "getOriginalGroupId", new Class<?>[0]));
		Assert.assertTrue(
			Objects.equals(
				existingUniversity.getUniversityCode(),
				ReflectionTestUtil.invoke(
					existingUniversity, "getOriginalUniversityCode",
					new Class<?>[0])));
	}

	protected University addUniversity() throws Exception {
		long pk = RandomTestUtil.nextLong();

		University university = _persistence.create(pk);

		university.setGroupId(RandomTestUtil.nextLong());

		university.setCompanyId(RandomTestUtil.nextLong());

		university.setUserId(RandomTestUtil.nextLong());

		university.setUserName(RandomTestUtil.randomString());

		university.setCreateDate(RandomTestUtil.nextDate());

		university.setModifiedDate(RandomTestUtil.nextDate());

		university.setName(RandomTestUtil.randomString());

		university.setUniversityCode(RandomTestUtil.randomString());

		university.setUrl(RandomTestUtil.randomString());

		university.setDataType();

		_universities.add(_persistence.update(university));

		return university;
	}

	private List<University> _universities = new ArrayList<University>();
	private UniversityPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;

}