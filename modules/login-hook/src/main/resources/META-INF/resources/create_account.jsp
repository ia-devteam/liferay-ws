<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="it.hiree.register.service.UniversityLocalServiceUtil"%>
<%@page import="it.hiree.register.model.University"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="it.hiree.hook.prop.PropValues"%>
<%@page import="it.hiree.register.constant.UniversityConstant"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ include file="/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

String openId = ParamUtil.getString(request, "openId");
boolean male = ParamUtil.getBoolean(request, "male", true);

Calendar birthdayCalendar = CalendarFactoryUtil.getCalendar();

birthdayCalendar.set(Calendar.MONTH, Calendar.JANUARY);
birthdayCalendar.set(Calendar.DATE, 1);
birthdayCalendar.set(Calendar.YEAR, 1970);

renderResponse.setTitle(LanguageUtil.get(request, "create-account"));

String hookRegister = PropValues.HOOK_REGISTER;
System.out.println(themeDisplay.getScopeGroupId());
%>
<c:choose>
      
	<c:when test = "<%= Validator.isNull(hookRegister) || GetterUtil.getBoolean(hookRegister) %>">
		<%
			List<University> universities = UniversityLocalServiceUtil.findByGroupId_DataType(themeDisplay.getScopeGroupId(), UniversityConstant.AVAILABLE);
						
			String homeURL = themeDisplay.getPortalURL();
		%>
		<portlet:actionURL name="/register/action" var="registerURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
			<portlet:param name="<%=Constants.CMD %>" value="CUSTOM_REGISTER" />
		</portlet:actionURL>
		
		<portlet:actionURL name="/register/action" var="authURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
			<portlet:param name="<%=Constants.CMD %>" value="AUTH" />
		</portlet:actionURL>
		
		<aui:form name="fm1" action="<%=registerURL.toString() %>" method="post">		
			<div id="<portlet:namespace/>step1" >
				<aui:input autoFocus="<%= true %>" name="name" label="name"/>
				<aui:input name="surName" label="Surname"></aui:input>
				<aui:input name="emailAddress"></aui:input>
				<aui:input name="password" type="password"></aui:input>
				<aui:input name="repassword" type="password" label="Re-password"></aui:input>
				
				<aui:button-row>
					<button class="btn btn-primary btn-default" type="button" name="next1" id="<portlet:namespace/>next1">Next</button>
				</aui:button-row>
			</div>
			<div id="<portlet:namespace/>step2" class="hide">
				<aui:button-row>
					<button class="btn btn-default" type="button" name="back" id="<portlet:namespace/>back1">Back</button>
					<button class="btn btn-primary btn-default" type="button" name="yes" id="<portlet:namespace/>yes">I'm student now</button>
					<button class="btn btn-primary btn-default" type="button" name="no" id="<portlet:namespace/>no">I'm finish</button>
					<aui:input type="hidden" name="finished" label="Finished"/>
				</aui:button-row>
			</div>
			<div id="<portlet:namespace/>step3" class="hide">
				<div id="universities"></div>
				<aui:select label="University" name="university">
					<%
						for(University university : universities){
							%>
								<aui:option label="<%=university.getName() %>" value="<%=university.getUniversityCode()%>" url="<%=university.getUrl()%>" />
							<%
						}
					%>
					
				</aui:select>
				<aui:input type="hidden" name="universityUrl" value="<%=(universities != null && !universities.isEmpty()) ? universities.get(0).getUrl() : StringPool.BLANK %>"/>
				<aui:button-row>
					<input type="checkbox" id="<portlet:namespace/>addUniversity" name="addUniversity">
					<label for="<portlet:namespace/>addUniversity"> My school is not listed, let me type my own</label>
					
				</aui:button-row>
				<aui:button-row>
					<button class="btn btn-default" type="button" name="back" id="<portlet:namespace/>back2">Back</button>
					<button class="btn btn-primary btn-default" type="button" name="next2" id="<portlet:namespace/>next2">Next</button>
				</aui:button-row>
			</div>
			
			<div id="<portlet:namespace/>step4a" class="hide">
				<aui:input type="text" name="userName" label="UserName"/>
				<aui:input type="password" name="secret" label="Password"/>
				<aui:button-row>
					<button class="btn btn-default" type="button" name="back" id="<portlet:namespace/>back3a">Back</button>
					<button class="btn btn-primary btn-default" type="button" name="register1" id="<portlet:namespace/>register1">Register</button>
				</aui:button-row>
			</div>
			
			<div id="<portlet:namespace/>step4b" class="hide">
				<aui:input type="text" name="universityName" label="University Name"/>
				<aui:input type="text" name="universityCode" label="University Code"/>
				<aui:input type="text" name="course" label="Course"/>
				<aui:button-row>
					<button class="btn btn-default" type="button" name="back" id="<portlet:namespace/>back3b">Back</button>
					<button class="btn btn-primary btn-default" type="button" name="register2" id="<portlet:namespace/>register2">Register</button>
				</aui:button-row>
			</div>
		</aui:form>
		<aui:script>
			AUI().use('aui-autocomplete', 'autocomplete-list', 'aui-base', 'aui-io-request', 'autocomplete-filters', 'autocomplete-highlighters', 'liferay-form', 'aui-form-validator', function(A) {
				
				var step1Elm =  A.one('#<portlet:namespace/>step1');
				var step2Elm =  A.one('#<portlet:namespace/>step2');
				var step3Elm =  A.one('#<portlet:namespace/>step3');
				var step4aElm =  A.one('#<portlet:namespace/>step4a');
				var step4bElm =  A.one('#<portlet:namespace/>step4b');
				var next1Btn = A.one('#<portlet:namespace/>next1');
				var next2Btn = A.one('#<portlet:namespace/>next2');
				var back1Btn = A.one('#<portlet:namespace/>back1');
				var back2Btn = A.one('#<portlet:namespace/>back2');
				var back3aBtn = A.one('#<portlet:namespace/>back3a');
				var back3bBtn = A.one('#<portlet:namespace/>back3b');
				var yesBtn = A.one('#<portlet:namespace/>yes');
				var noBtn = A.one('#<portlet:namespace/>no');
				var register1Btn = A.one('#<portlet:namespace/>register1');
				var register2Btn = A.one('#<portlet:namespace/>register2');
				var finishedField = A.one('#<portlet:namespace/>finished');
				var universityUrlField = A.one('#<portlet:namespace/>universityUrl');
				var universitySelect = A.one('#<portlet:namespace/>university');
				var addUniversityBtn = A.one('#<portlet:namespace/>addUniversity');
				var fm1 = A.one('#<portlet:namespace/>fm1');
				
				next1Btn.on('click', function(event) {
					fmValidator1.validate();
					if(fmValidator1.hasErrors() > 0){
						return;
					}
		           	step1Elm.hide();
					step2Elm.show();
			        
				});
				
				back1Btn.on('click', function(event) {
					step1Elm.show();
					step2Elm.hide();
				});
				
				yesBtn.on('click', function(event) {
					step2Elm.hide();
					step3Elm.show();
					finishedField.val('1');
				});
				
				noBtn.on('click', function(event) {
					step2Elm.hide();
					step3Elm.show();
					finishedField.val('0');
				});
				
				back2Btn.on('click', function(event) {
					step2Elm.show();
					step3Elm.hide();
				});
				
				next2Btn.on('click', function(event) {
					fmValidator3.validate();
					if(fmValidator3.hasErrors() > 0){
						return;
					}
					step3Elm.hide();
					step4aElm.show();
				});
				
				back3aBtn.on('click', function(event) {
					step3Elm.show();
					step4aElm.hide();
				});
				
				addUniversityBtn.on('click', function(event) {
					step3Elm.hide();
					step4bElm.show();
				});
				
				back3bBtn.on('click', function(event) {
					step3Elm.show();
					step4bElm.hide();
					document.getElementById("<portlet:namespace/>addUniversity").checked = false;
				});
				
				universitySelect.on('change', function(event) {
					var url = A.one('option:selected', this).attr('url');
					universityUrlField.val(url);
				});
				
				var universityInput = A.one('#universities');
				var homeURL = '<%=homeURL.toString() %>';
				
				register1Btn.on('click', function(event) {
					fmValidator4a.validate();
					if(fmValidator4a.hasErrors() > 0){
						return;
					}
		        	var url = '<%=registerURL.toString() %>';
		        	var url = '<%=registerURL.toString() %>';
		        	var authUrl = '<%=authURL.toString() %>';
		        	var name = A.one('#<portlet:namespace/>name').val();
					var surName = A.one('#<portlet:namespace/>surName').val();
					var password = A.one('#<portlet:namespace/>password').val();
					var emailAddress = A.one('#<portlet:namespace/>emailAddress').val();
					var finished = A.one('#<portlet:namespace/>finished').val();
					var university = A.one('#<portlet:namespace/>university').val();
					var universityCode = A.one('#<portlet:namespace/>university').val();
					var universityName = A.one('#<portlet:namespace/>universityName').val();
					var course = A.one('#<portlet:namespace/>course').val();
					var userName = A.one('#<portlet:namespace/>userName').val();
					var secret = A.one('#<portlet:namespace/>secret').val();
					var universityUrl = universityUrlField.val();
		        	A.io.request(url, {
		        		method:	'POST',
						dataType: 'json',
						
						data: {
							<portlet:namespace/>name:name,
							<portlet:namespace/>surName:surName,
							<portlet:namespace/>password:password,
							<portlet:namespace/>emailAddress:emailAddress,
							<portlet:namespace/>finished:finished,
							<portlet:namespace/>university:university,
							<portlet:namespace/>universityUrl:universityUrl,
							<portlet:namespace/>authRequired: true,
							<portlet:namespace/>userName: userName,
							<portlet:namespace/>secret: secret
						},
						on: {
							success: function() {
								//var data = JSON.stringify(this.get('responseData'));
								var data = this.get('responseData');
								if(parseInt(data.mappingUserId) > 0){
									alert(JSON.stringify(data));
									window.location.href = homeURL;
								}else{
									alert(JSON.stringify(data));
								}
							}
						}
					});
		        });
		        
		        register2Btn.on('click', function(event) {
		        	fmValidator4b.validate();
					if(fmValidator4b.hasErrors() > 0){
						return;
					}
		        	var url = '<%=registerURL.toString() %>';
		        	var name = A.one('#<portlet:namespace/>name').val();
					var surName = A.one('#<portlet:namespace/>surName').val();
					var password = A.one('#<portlet:namespace/>password').val();
					var emailAddress = A.one('#<portlet:namespace/>emailAddress').val();
					var finished = A.one('#<portlet:namespace/>finished').val();
					var university = A.one('#<portlet:namespace/>university').val();
					var universityCode = A.one('#<portlet:namespace/>university').val();
					var universityName = A.one('#<portlet:namespace/>universityName').val();
					var course = A.one('#<portlet:namespace/>course').val();
					
		        	A.io.request(url, {
		        		method:	'POST',
						dataType: 'json',
						data: {
							<portlet:namespace/>name:name,
							<portlet:namespace/>surName:surName,
							<portlet:namespace/>password:password,
							<portlet:namespace/>emailAddress:emailAddress,
							<portlet:namespace/>finished:finished,
							<portlet:namespace/>universityName:universityName,
							<portlet:namespace/>universityCode:universityCode,
							<portlet:namespace/>course:course,
							<portlet:namespace/>authRequired: false
						},
						on: {
							success: function() {
								var data = this.get('responseData');
								if(parseInt(data.mappingUserId) > 0){
									alert(JSON.stringify(data));
									window.location.href = homeURL;
								}else{
									alert(JSON.stringify(data));
								}
							}
						}
					});
		        });
		        
				var rules1 = {
				
					<portlet:namespace/>name: {
					  rangeLength: [2, 50],
					  required: true
					},
					
					<portlet:namespace/>surName: {
					  rangeLength: [2, 50],
					  required: true
					},
					
					<portlet:namespace/>emailAddress: {
						email: true,
						required: true
					},
					<portlet:namespace/>repassword: {
						
						equalTo: '#<portlet:namespace/>password',
						required: true
					}
					
				};

				var fieldStrings1 = {
					<portlet:namespace/>name: {
				  		required: 'Please enter your name.'
					},
					
					<portlet:namespace/>surName: {
						required: 'Please enter your surName.'
					},
				
					<portlet:namespace/>email: {
				  		required: 'Please enter your email.'
					},
					
					<portlet:namespace/>password: {
				  		required: 'Please enter your password.'
					},
					
					<portlet:namespace/>repassword: {
						
						equalTo: 'Repassword incorrect.'
					}
				};
				
				var rules3 = {
				
					<portlet:namespace/>university: {
					  required: true
					}
				};

				var fieldStrings3 = {
					<portlet:namespace/>university: {
				  		required: 'Please select your university.'
					}
				};
				
				var rules4a = {
				
					<portlet:namespace/>userName: {
						required: true
					},
					<portlet:namespace/>secret: {
						required: true
					}
					
				};

				var fieldStrings4a = {
					
					<portlet:namespace/>userName: {
						required: 'Please enter your username.'
					},
					<portlet:namespace/>secret: {
						required: 'Please enter your password.'
					}
				};
				
				var rules4b = {
				
					<portlet:namespace/>universityName: {
						required: true
					}
					
				};

				var fieldStrings4b = {
					
					<portlet:namespace/>universityName: {
						required: 'Please enter your university name.'
					}
				};
				

				var fmValidator1 = new A.FormValidator(
					{
						boundingBox: '#<portlet:namespace/>fm1',
						fieldStrings: fieldStrings1,
						rules: rules1,
						showAllMessages: true
					}
				);
				
				var fmValidator3 = new A.FormValidator(
					{
						boundingBox: '#<portlet:namespace/>fm1',
						fieldStrings: fieldStrings3,
						rules: rules3,
						showAllMessages: true
					}
				);
				
				var fmValidator4a = new A.FormValidator(
					{
						boundingBox: '#<portlet:namespace/>fm1',
						fieldStrings: fieldStrings4a,
						rules: rules4a,
						showAllMessages: true
					}
				);
				
				var fmValidator4b = new A.FormValidator(
					{
						boundingBox: '#<portlet:namespace/>fm1',
						fieldStrings: fieldStrings4b,
						rules: rules4b,
						showAllMessages: true
					}
				);
			});
			
		</aui:script>
	</c:when>
	
	<c:otherwise>
		<c:if test="<%= Validator.isNotNull(openId) %>">
			<div class="alert alert-info">
				<liferay-ui:message arguments="<%= openId %>" key="you-are-about-to-create-an-account-with-openid-x" translateArguments="<%= false %>" />
			</div>
		</c:if>
		
		<portlet:actionURL name="/login/create_account" secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="createAccountURL" windowState="<%= LiferayWindowState.MAXIMIZED.toString() %>">
			<portlet:param name="mvcRenderCommandName" value="/login/create_account" />
		</portlet:actionURL>
		
		<aui:form action="<%= createAccountURL %>" method="post" name="fm">
			<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
			<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.ADD %>" />
			<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
			<aui:input name="openId" type="hidden" value="<%= openId %>" />
		
			<liferay-ui:error exception="<%= AddressCityException.class %>" message="please-enter-a-valid-city" />
			<liferay-ui:error exception="<%= AddressStreetException.class %>" message="please-enter-a-valid-street" />
			<liferay-ui:error exception="<%= AddressZipException.class %>" message="please-enter-a-valid-postal-code" />
			<liferay-ui:error exception="<%= CaptchaConfigurationException.class %>" message="a-captcha-error-occurred-please-contact-an-administrator" />
			<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-create-user-account-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= ContactBirthdayException.class %>" message="please-enter-a-valid-birthday" />
			<liferay-ui:error exception="<%= ContactNameException.MustHaveFirstName.class %>" message="please-enter-a-valid-first-name" />
			<liferay-ui:error exception="<%= ContactNameException.MustHaveLastName.class %>" message="please-enter-a-valid-last-name" />
			<liferay-ui:error exception="<%= ContactNameException.MustHaveValidFullName.class %>" message="please-enter-a-valid-first-middle-and-last-name" />
			<liferay-ui:error exception="<%= DuplicateOpenIdException.class %>" message="a-user-with-that-openid-already-exists" />
			<liferay-ui:error exception="<%= EmailAddressException.class %>" message="please-enter-a-valid-email-address" />
		
			<liferay-ui:error exception="<%= GroupFriendlyURLException.class %>">
		
				<%
				GroupFriendlyURLException gfurle = (GroupFriendlyURLException)errorException;
				%>
		
				<c:if test="<%= gfurle.getType() == GroupFriendlyURLException.DUPLICATE %>">
					<liferay-ui:message key="the-screen-name-you-requested-is-associated-with-an-existing-friendly-url" />
				</c:if>
			</liferay-ui:error>
		
			<liferay-ui:error exception="<%= NoSuchCountryException.class %>" message="please-select-a-country" />
			<liferay-ui:error exception="<%= NoSuchListTypeException.class %>" message="please-select-a-type" />
			<liferay-ui:error exception="<%= NoSuchRegionException.class %>" message="please-select-a-region" />
			<liferay-ui:error exception="<%= PhoneNumberException.class %>" message="please-enter-a-valid-phone-number" />
			<liferay-ui:error exception="<%= PhoneNumberExtensionException.class %>" message="please-enter-a-valid-phone-number-extension" />
			<liferay-ui:error exception="<%= RequiredFieldException.class %>" message="please-fill-out-all-required-fields" />
			<liferay-ui:error exception="<%= TermsOfUseException.class %>" message="you-must-agree-to-the-terms-of-use" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeDuplicate.class %>" message="the-email-address-you-requested-is-already-taken" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeNull.class %>" message="please-enter-an-email-address" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBePOP3User.class %>" message="the-email-address-you-requested-is-reserved" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeReserved.class %>" message="the-email-address-you-requested-is-reserved" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotUseCompanyMx.class %>" message="the-email-address-you-requested-is-not-valid-because-its-domain-is-reserved" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustValidate.class %>" message="please-enter-a-valid-email-address" />
			<liferay-ui:error exception="<%= UserIdException.MustNotBeNull.class %>" message="please-enter-a-user-id" />
			<liferay-ui:error exception="<%= UserIdException.MustNotBeReserved.class %>" message="the-user-id-you-requested-is-reserved" />
		
			<liferay-ui:error exception="<%= UserPasswordException.MustBeLonger.class %>">
		
				<%
				UserPasswordException.MustBeLonger upe = (UserPasswordException.MustBeLonger)errorException;
				%>
		
				<liferay-ui:message arguments="<%= String.valueOf(upe.minLength) %>" key="that-password-is-too-short" translateArguments="<%= false %>" />
			</liferay-ui:error>
		
			<liferay-ui:error exception="<%= UserPasswordException.MustComplyWithModelListeners.class %>" message="that-password-is-invalid-please-enter-a-different-password" />
		
			<liferay-ui:error exception="<%= UserPasswordException.MustComplyWithRegex.class %>">
		
				<%
				UserPasswordException.MustComplyWithRegex upe = (UserPasswordException.MustComplyWithRegex)errorException;
				%>
		
				<liferay-ui:message arguments="<%= HtmlUtil.escape(upe.regex) %>" key="that-password-does-not-comply-with-the-regular-expression" translateArguments="<%= false %>" />
			</liferay-ui:error>
		
			<liferay-ui:error exception="<%= UserPasswordException.MustMatch.class %>" message="the-passwords-you-entered-do-not-match" />
			<liferay-ui:error exception="<%= UserPasswordException.MustNotBeNull.class %>" message="the-password-cannot-be-blank" />
			<liferay-ui:error exception="<%= UserPasswordException.MustNotBeTrivial.class %>" message="that-password-uses-common-words-please-enter-a-password-that-is-harder-to-guess-i-e-contains-a-mix-of-numbers-and-letters" />
			<liferay-ui:error exception="<%= UserPasswordException.MustNotContainDictionaryWords.class %>" message="that-password-uses-common-dictionary-words" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeDuplicate.class %>" focusField="screenName" message="the-screen-name-you-requested-is-already-taken" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNull.class %>" focusField="screenName" message="the-screen-name-cannot-be-blank" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNumeric.class %>" focusField="screenName" message="the-screen-name-cannot-contain-only-numeric-values" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeReserved.class %>" message="the-screen-name-you-requested-is-reserved" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeReservedForAnonymous.class %>" focusField="screenName" message="the-screen-name-you-requested-is-reserved-for-the-anonymous-user" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeUsedByGroup.class %>" focusField="screenName" message="the-screen-name-you-requested-is-already-taken-by-a-site" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustProduceValidFriendlyURL.class %>" focusField="screenName" message="the-screen-name-you-requested-must-produce-a-valid-friendly-url" />
		
			<liferay-ui:error exception="<%= UserScreenNameException.MustValidate.class %>" focusField="screenName">
		
				<%
				UserScreenNameException.MustValidate usne = (UserScreenNameException.MustValidate)errorException;
				%>
		
				<liferay-ui:message key="<%= usne.screenNameValidator.getDescription(locale) %>" />
			</liferay-ui:error>
		
			<liferay-ui:error exception="<%= WebsiteURLException.class %>" message="please-enter-a-valid-url" />
		
			<c:if test='<%= SessionMessages.contains(request, "openIdUserInformationMissing") %>'>
				<div class="alert alert-info">
					<liferay-ui:message key="you-have-successfully-authenticated-please-provide-the-following-required-information-to-access-the-portal" />
				</div>
			</c:if>
		
			<aui:model-context model="<%= Contact.class %>" />
		
			<aui:fieldset column="<%= true %>">
				<aui:col width="<%= 50 %>">
		
					<%
					Boolean autoGenerateScreenName = PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.USERS_SCREEN_NAME_ALWAYS_AUTOGENERATE);
					%>
		
					<c:if test="<%= !autoGenerateScreenName %>">
						<aui:input autoFocus="<%= true %>" model="<%= User.class %>" name="screenName">
		
							<%
							ScreenNameValidator screenNameValidator = ScreenNameValidatorFactory.getInstance();
							%>
		
							<c:if test="<%= Validator.isNotNull(screenNameValidator.getAUIValidatorJS()) %>">
								<aui:validator errorMessage="<%= screenNameValidator.getDescription(locale) %>" name="custom">
									<%= screenNameValidator.getAUIValidatorJS() %>
								</aui:validator>
							</c:if>
						</aui:input>
					</c:if>
		
					<aui:input autoFocus="<%= autoGenerateScreenName %>" model="<%= User.class %>" name="emailAddress">
						<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.USERS_EMAIL_ADDRESS_REQUIRED) %>">
							<aui:validator name="required" />
						</c:if>
					</aui:input>
		
					<liferay-ui:user-name-fields />
				</aui:col>
		
				<aui:col width="<%= 50 %>">
					<c:if test="<%= PropsValues.LOGIN_CREATE_ACCOUNT_ALLOW_CUSTOM_PASSWORD %>">
						<aui:input label="password" name="password1" size="30" type="password" value="">
							<aui:validator name="required" />
						</aui:input>
		
						<aui:input label="enter-again" name="password2" size="30" type="password" value="">
							<aui:validator name="equalTo">
								'#<portlet:namespace />password1'
							</aui:validator>
		
							<aui:validator name="required" />
						</aui:input>
					</c:if>
		
					<c:choose>
						<c:when test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.FIELD_ENABLE_COM_LIFERAY_PORTAL_KERNEL_MODEL_CONTACT_BIRTHDAY) %>">
							<aui:input name="birthday" value="<%= birthdayCalendar %>" />
						</c:when>
						<c:otherwise>
							<aui:input name="birthdayMonth" type="hidden" value="<%= Calendar.JANUARY %>" />
							<aui:input name="birthdayDay" type="hidden" value="1" />
							<aui:input name="birthdayYear" type="hidden" value="1970" />
						</c:otherwise>
					</c:choose>
		
					<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.FIELD_ENABLE_COM_LIFERAY_PORTAL_KERNEL_MODEL_CONTACT_MALE) %>">
						<aui:select label="gender" name="male">
							<aui:option label="male" value="1" />
							<aui:option label="female" selected="<%= !male %>" value="0" />
						</aui:select>
					</c:if>
		
					<c:if test="<%= captchaConfiguration.createAccountCaptchaEnabled() %>">
						<portlet:resourceURL id="/login/captcha" var="captchaURL" />
		
						<liferay-captcha:captcha
							url="<%= captchaURL %>"
						/>
					</c:if>
				</aui:col>
			</aui:fieldset>
		
			<aui:button-row>
				<aui:button type="submit" />
			</aui:button-row>
		</aui:form>
	</c:otherwise>

</c:choose>



<%@ include file="/navigation.jspf" %>